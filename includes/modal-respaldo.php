<!--PLAN-->
	<section class="uk-container padding-70-0" id="plan">
		<div class="cohete" style="
		position: absolute;left:0;">
			<img style="
			height: 480px;
			float:left;
			margin:0;
			padding:0;
			left:-50px;" src="./img/design/cohete.png">
		</div>
		<div class="uk-grid-divider uk-child-width-expand@s margin-left-0 padding-30-0" uk-grid>
		    <div class="aling-center">
		    	<h1 class="title padding-top-20 padding-bottom-20">
		    		Hagamos un plan</h1>
		    	<h6 class="font-bold padding-bottom-10">¡Recibe asesoría gratuita!</h6>
				Si estás buscando abrir un restaurante, una nueva sucursal o simplemente necesitas ampliar tu capacidad de producción, nosotros podemos ayudarte a llegar hasta el espacio sideral.

				<button class="uk-button uk-button-default button-border margin-top-30" type="button" uk-toggle="target: #modal-group-1"> ¡Comencemos! </button>

				<!-- =============================================== -->
				<!-- =============  MODAL FORMULARIO =============== -->
				<!-- =============================================== -->
						<div id="modal-group-1" class="uk-transition-fade" uk-modal uk-position-cover>
							<!--div class="cohete-modal" style="
								position: absolute;left:-6em;z-index:99">
								<img style="height:89vh;" src="./img/design/cohete.png">
							</div-->
						    <div class="uk-modal-dialog padding-top-50"
						    style="height:80vh; width:80vw">
						        <button class="uk-modal-close-default" type="button" uk-close></button>
						        <div class="uk-modal-body padding-top-60">
						        	<p class="title" style="font-size:46px; font-weight:800">
						        		Hagamos un plan
						        	</p>
						        	<div class="title-2">¿Estás listo?</div>
						        	<div class="title padding-top-100">¿Cuál es tu nombre?</div>
									<div class="uk-margin uk-flex uk-flex-center" uk-grid>
											<div style="border: solid #ff7a0b 2px;border-radius: 20px; width:600px" >
									    		<input class="cero uk-input aling-center" type="text" placeholder="Escribe tu nombre aquí" style="border: solid transparent;width:90%; float: left; width: 94%; ">
									    		<span>
									    		<a href="#modal-group-2" class="cero" uk-toggle>
									    			<span class="cero" style="background-color:#ff7a0b;
													    color: #fff;
													    border-radius: 100%;
													    width: 42px;
													    height: 42px;
													    position: absolute;
													    z-index: 99;
													    margin-right: 20px;" uk-icon="icon:chevron-right; ratio: 2"></span>
												</a>
									    		</span>
									    	</div>
									</div>
						        </div>
						        
						        <!------footer modal--->
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
								    position: absolute;
								    margin-bottom: 30px;
								    bottom: 0;
								    width:100%;">
						        	<!------botones--->
									<div class="uk-flex uk-flex-center uk-align-left cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-1" class="uk-align-left cero" uk-toggle style="margin-left:0"><span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo</a>
									</div>
									<div class="uk-width-expand@m">
										<div class="uk-flex uk-flex-center width-100">
										    <img src="./img/design/logo-footer.png">
										</div>
									</div>
									<div class="uk-flex uk-flex-center uk-align-right cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										
									</div>
									
						        </div>
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 " style="
								    position: absolute;
								    margin-bottom: 0;
								    bottom: 0;
								    width:100%;">
								    <div class="uk-width-1-1 width-100">
										<p style="font-size:8px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut </p>
									</div>
								</div>
						    </div>
						</div>

						<div id="modal-group-2" class="uk-transition-fade" uk-modal uk-position-cover>
							<!--div class="cohete-modal" style="
								position: absolute;left:-6em;z-index:99">
								<img style="height:89vh;" src="./img/design/cohete.png">
							</div-->
						    <div class="uk-modal-dialog padding-top-100"
						    style="height:80vh; width:80vw">
						        <button class="uk-modal-close-default" type="button" uk-close></button>
						        <div class="uk-modal-body padding-top-60">
						        	<p class="title padding-top-60" style="font-size:46px; font-weight:800">
						        		Hola, Valdomero
						        	</p>
						        	
						        	<div class="title-2 padding-top-40">en Spacefood queremos ser parte de tu órbita.</div>
									<div class="uk-margin uk-flex uk-flex-center" uk-grid>
										<div style="width:600px" >
									    		<p class="uk-text-center">
									    			Tu viaje en Spacefood comienza aquí.
									    		</p>
									    </div>
									</div>
						        </div>
						        <!------footer modal--->
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
								    position: absolute;
								    margin-bottom: 30px;
								    bottom: 0;
								    width:100%;">
						        	<!------botones--->
									<div class="uk-flex uk-flex-center uk-align-left cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-1" class="uk-align-left cero" uk-toggle style="margin-left:0"><span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo</a>
									</div>
									<div class="uk-width-expand@m">
										<div class="uk-flex uk-flex-center width-100">
										    <img src="./img/design/logo-footer.png">
										</div>
									</div>
									<div class="uk-flex uk-flex-center uk-align-right cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-3" class="uk-align-right cero" uk-toggle style="margin-left:0">Next <span uk-icon="icon:chevron-right; ratio:1.5"></span></a>
									</div>
									
						        </div>
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 " style="
								    position: absolute;
								    margin-bottom: 0;
								    bottom: 0;
								    width:100%;">
								    <div class="uk-width-1-1 width-100">
										<p style="font-size:8px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut </p>
									</div>
								</div>
						    </div>
						</div>

						<div id="modal-group-3" class="uk-transition-fade" uk-modal uk-position-cover>
							<!--div class="cohete-modal" style="
								position: absolute;left:-6em;z-index:99">
								<img style="height:89vh;" src="./img/design/cohete.png">
							</div-->
						    <div class="uk-modal-dialog padding-top-60"
						    style="height:80vh; width:80vw">
						        <button class="uk-modal-close-default" type="button" uk-close></button>
						        <div class="uk-modal-body">
						        	<div class="uk-flex uk-flex-center">
						        		<div style="
						        			border-radius:100%; 
						        			height:50px;
						        			width:50px;
						        			background-color:#ff7a0b"></div>
						        	</div>
						        	<p class="uk-text-center padding-top-60">
						        		Esta es tu capsula de viaje, da click en las opciones de abajo para empezar
						        	</p>
						        	<p class="title padding-top-10" style="">
						        		Selecciona la opción que estás buscando
						        	</p>
						        	<!----las opciones---->
									<div class="uk-container uk-container-small aling-center">
										<div uk-grid class="uk-text-center" style="margin-left:0">
											<div class="uk-width-1-2@s uk-width-1-4@m tenemos"
											style="border-right:solid 1px #ff7a0b">
												<a class="container-tenemos" href="#">
													<div class="uk-flex-center">
														<div class="img-tenemos transicion tenemos-1"></div>
													</div>
													<h6 class="font-bold">Consumibles</h6>
												</a>
											</div>
											<div class="uk-width-1-2@s uk-width-1-4@m tenemos"
											style="border-right:solid 1px #ff7a0b">
												<a class="container-tenemos" href="#">
													<div class="uk-flex-center">
														<div class="img-tenemos transicion tenemos-2"></div>
													</div>
													<h6 class="font-bold">Venta de equipo</h6>
												</a>
											</div>
											<div class="uk-width-1-2@s uk-width-1-4@m tenemos"
											style="border-right:solid 1px #ff7a0b">
												<a class="container-tenemos" href="#">
													<div class="uk-flex-center">
														<div class="img-tenemos transicion tenemos-3"></div>
													</div>
													<h6 class="font-bold">Venta de equipo</h6>
												</a>
											</div>
											<div class="uk-width-1-2@s uk-width-1-4@m tenemos">
												<a class="container-tenemos" href="#">
													<div class="uk-flex-center">
														<div class="img-tenemos transicion tenemos-4"></div>
													</div>
													<h6 class="font-bold">Venta de equipo</h6>
												</a>
											</div>
										</div>
										<div uk-grid class="uk-text-center margin-left-0">
											
										</div>
									</div>

						        </div>
						        
						        <!------footer modal--->
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
								    position: absolute;
								    margin-bottom: 30px;
								    bottom: 0;
								    width:100%;">
						        	<!------botones--->
									<div class="uk-flex uk-flex-center uk-align-left cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-2" class="uk-align-left cero" uk-toggle style="margin-left:0"><span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo</a>
									</div>
									<div class="uk-width-expand@m">
										<div class="uk-flex uk-flex-center width-100">
										    <img src="./img/design/logo-footer.png">
										</div>
									</div>
									<div class="uk-flex uk-flex-center uk-align-right cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-4" class="uk-align-right cero" uk-toggle style="margin-left:0">Next <span uk-icon="icon:chevron-right; ratio:1.5"></span></a>
									</div>
									
						        </div>
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 " style="
								    position: absolute;
								    margin-bottom: 0;
								    bottom: 0;
								    width:100%;">
								    <div class="uk-width-1-1 width-100">
										<p style="font-size:8px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut </p>
									</div>
								</div>
						    </div>
						</div>

						<div id="modal-group-4" class="uk-transition-fade" uk-modal uk-position-cover>
							<!--div class="cohete-modal" style="
								position: absolute;left:-6em;z-index:99">
								<img style="height:89vh;" src="./img/design/cohete.png">
							</div-->
						    <div class="uk-modal-dialog padding-top-60"
						    style="height:80vh; width:80vw">
						        <button class="uk-modal-close-default" type="button" uk-close></button>
						        <div class="uk-modal-body">
						        	<div class="uk-flex uk-flex-center">
						        		<div style="">1/6</div>
						        	</div>
						        	<p class="uk-text-center">
						        	</p>
						        	<p class="title padding-top-60" style="">
						        		Para cuantos necesitas<br>el equipo
						        	</p>
						        	<!----las opciones---->
									<div class="uk-container uk-container-small aling-center">
										<div uk-grid class="uk-flex uk-flex-center uk-text-center" style="margin-left:0">
											<div class="uk-width-1-2@s uk-width-1-4@m tenemos"
											style="border-right:solid 1px #ff7a0b">
												<a class="container-tenemos" href="#">
													<div class="uk-flex uk-flex-center">
														<div class="uk-flex uk-flex-middle uk-flex-center transicion circle">
															Inmediato
														</div>
													</div>
												</a>
											</div>
											<div class="uk-width-1-2@s uk-width-1-4@m tenemos"
											style="border-right:solid 1px #ff7a0b">
												<a class="container-tenemos" href="#">
													<div class="uk-flex uk-flex-center">
														<div class="uk-flex uk-flex-middle uk-flex-center transicion circle">
															En 3 meses o menos
														</div>
													</div>
												</a>
											</div>
											<div class="uk-width-1-2@s uk-width-1-4@m tenemos"
											style="border-right:solid 1px #ff7a0b">
												<a class="container-tenemos" href="#">
													<div class="uk-flex uk-flex-center">
														<div class="uk-flex uk-flex-middle uk-flex-center transicion circle">
															Entre 3 y 6 meses
														</div>
													</div>
												</a>
											</div>
											<div class="uk-width-1-2@s uk-width-1-4@m tenemos">
												<a class="container-tenemos" href="#">
													<div class="uk-flex uk-flex-center">
														<div class="uk-flex uk-flex-middle uk-flex-center transicion circle">
															Entre 6 meses y un año
														</div>
													</div>
												</a>
											</div>

										</div>
										<div uk-grid class="uk-text-center margin-left-0">
											
										</div>
									</div>

						        </div>
						        <!------footer modal--->
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
								    position: absolute;
								    margin-bottom: 30px;
								    bottom: 0;
								    width:100%;">
						        	<!------botones--->
									<div class="uk-flex uk-flex-center uk-align-left cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-3" class="uk-align-left cero" uk-toggle style="margin-left:0"><span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo</a>
									</div>
									<div class="uk-width-expand@m">
										<div class="uk-flex uk-flex-center width-100">
										    <img src="./img/design/logo-footer.png">
										</div>
									</div>
									<div class="uk-flex uk-flex-center uk-align-right cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-5" class="uk-align-right cero" uk-toggle style="margin-left:0">Next <span uk-icon="icon:chevron-right; ratio:1.5"></span></a>
									</div>
									
						        </div>
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 " style="
								    position: absolute;
								    margin-bottom: 0;
								    bottom: 0;
								    width:100%;">
								    <div class="uk-width-1-1 width-100">
										<p style="font-size:8px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut </p>
									</div>
								</div>
						    </div>
						</div>

						<div id="modal-group-5" class="uk-transition-fade" uk-modal uk-position-cover>
							<!--div class="cohete-modal" style="
								position: absolute;left:-6em;z-index:99">
								<img style="height:89vh;" src="./img/design/cohete.png">
							</div-->
						    <div class="uk-modal-dialog padding-top-60"
						    style="height:80vh; width:80vw">
						        <button class="uk-modal-close-default" type="button" uk-close></button>
						        <div class="uk-modal-body">
						        	<div class="uk-flex uk-flex-center">
						        		<div style="">2/6</div>
						        	</div>
						        	<p class="title padding-top-60" style="">
						        		¿Cómo defines el status de tu proyecto?
						        	</p>
						        	<!----las opciones---->
									<div class="uk-container uk-container-small aling-center">
										<div uk-grid class="uk-text-center" style="margin-left:0">
											<div class="uk-width-1-3@m tenemos"
											style="border-right:solid 1px #ff7a0b">
												<a class="container-tenemos" href="#">
													<div class="uk-flex-center">
														<div class="img-tenemos transicion tenemos-1"></div>
													</div>
													<h6 class="font-bold">Nuevo</h6>
													<div>Estoy iniciando mi primer punto de venta desde cero. </div>
												</a>
											</div>
											<div class="uk-width-1-3@m tenemos"
											style="border-right:solid 1px #ff7a0b">
												<a class="container-tenemos" href="#">
													<div class="uk-flex-center">
														<div class="img-tenemos transicion tenemos-2"></div>
													</div>
													<h6 class="font-bold">Crecimiento</h6>
													<div>Quiero abrir una nueva sucursal para tener de 2 a 5 puntos de venta.</div>
												</a>
											</div>
											<div class="uk-width-1-3@m tenemos">
												<a class="container-tenemos" href="#">
													<div class="uk-flex-center">
														<div class="img-tenemos transicion tenemos-4"></div>
													</div>
													<h6 class="font-bold">Exploción</h6>
													<div>Ya tengo más de 5 sucursales operando y queremos abrir más puntos de venta.</div>
												</a>
											</div>
										</div>
										<div uk-grid class="uk-text-center margin-left-0">
											
										</div>
									</div>

						        </div>
						        <!------footer modal--->
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
								    position: absolute;
								    margin-bottom: 30px;
								    bottom: 0;
								    width:100%;">
						        	<!------botones--->
									<div class="uk-flex uk-flex-center uk-align-left cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-4" class="uk-align-left cero" uk-toggle style="margin-left:0"><span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo</a>
									</div>
									<div class="uk-width-expand@m">
										<div class="uk-flex uk-flex-center width-100">
										    <img src="./img/design/logo-footer.png">
										</div>
									</div>
									<div class="uk-flex uk-flex-center uk-align-right cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-6" class="uk-align-right cero" uk-toggle style="margin-left:0">Next <span uk-icon="icon:chevron-right; ratio:1.5"></span></a>
									</div>
									
						        </div>
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 " style="
								    position: absolute;
								    margin-bottom: 0;
								    bottom: 0;
								    width:100%;">
								    <div class="uk-width-1-1 width-100">
										<p style="font-size:8px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut </p>
									</div>
								</div>
						    </div>
						</div>
						
						<div id="modal-group-6" class="uk-transition-fade" uk-modal uk-position-cover>
							<!--div class="cohete-modal" style="
								position: absolute;left:-6em;z-index:99">
								<img style="height:89vh;" src="./img/design/cohete.png">
							</div-->
						    <div class="uk-modal-dialog padding-top-60"
						    style="height:80vh; width:80vw">
						        <button class="uk-modal-close-default" type="button" uk-close></button>
						        <div class="uk-modal-body">
						        	<div class="uk-flex uk-flex-center">
						        		<div style="">3/6</div>
						        	</div>
						        	<p class="title padding-top-60" style="">
						        		¿Cuál equipo estás buscando?
						        	</p>
						        	<!----las opciones---->
									<div class="uk-container uk-container-small aling-center">
										<div uk-grid class="uk-text-center" style="margin-left:0">
	
											<div class="uk-container aling-center" id="menu2">
												<div uk-grid class="uk-text-center" style="margin-left:0">
													<div class="uk-width-1-5@m item-container">
														<a class="container-item">
															<div class="uk-flex-center">
																<div class="img-element transicion element-1"></div>
																<h6 class="font-bold cero">Horno de pizza</h6>
															</div>
														</a>
													</div>
													<div class="uk-width-1-5@m item-container">
														<a class="container-item">
															<div class="uk-flex-center">
																<div class="img-element transicion element-1"></div>
																<h6 class="font-bold cero">Maquinas de hielo</h6>
															</div>
														</a>
													</div>
													<div class="uk-width-1-5@m item-container">
														<a class="container-item">
															<div class="uk-flex-center">
																<div class="img-element transicion element-1"></div>
																<h6 class="font-bold cero">Freidora</h6>
															</div>
														</a>
													</div>
													<div class="uk-width-1-5@m item-container">
														<a class="container-item">
															<div class="uk-flex-center">
																<div class="img-element transicion element-1"></div>
																<h6 class="font-bold cero">Maquinas de helados</h6>
															</div>
														</a>
													</div>
													<div class="uk-width-1-5@m item-container">
														<a class="container-item">
															<div class="uk-flex-center">
																<div class="img-element transicion element-1"></div>
																<h6 class="font-bold cero">Freidora</h6>
															</div>
														</a>
													</div>
												</div>
												<div uk-grid class="uk-text-center margin-left-0">
													
												</div>
											</div>
										
										</div>
									</div>
									<!----input---->
										<div class="cero uk-margin uk-flex uk-flex-center" uk-grid>
											<p class="cero title" style="">
						        				¿Otro?</p>
										</div>
										<div class="cero uk-margin uk-flex uk-flex-center" uk-grid>
											<div style="border: solid #ff7a0b 2px;border-radius: 20px; width:600px" >
									    		<input class="cero uk-input aling-center" type="text" placeholder="Describelo aquí" style="border: solid transparent;width:90%; float: left; width: 94%; ">
									    		<span>
									    		<a href="#modal-group-2" class="cero" uk-toggle>
									    			<span class="cero" style="background-color:#ff7a0b;
													    color: #fff;
													    border-radius: 100%;
													    width: 42px;
													    height: 42px;
													    position: absolute;
													    z-index: 99;
													    margin-right: 20px;" uk-icon="icon:chevron-right; ratio: 2"></span>
												</a>
									    		</span>
									    	</div>
										</div>
						        </div>
						        <!------footer modal--->
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
								    position: absolute;
								    margin-bottom: 30px;
								    bottom: 0;
								    width:100%;">
						        	<!------botones--->
									<div class="uk-flex uk-flex-center uk-align-left cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-6" class="uk-align-left cero" uk-toggle style="margin-left:0"><span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo</a>
									</div>
									<div class="uk-width-expand@m">
										<div class="uk-flex uk-flex-center width-100">
										    <img src="./img/design/logo-footer.png">
										</div>
									</div>
									<div class="uk-flex uk-flex-center uk-align-right cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-7" class="uk-align-right cero" uk-toggle style="margin-left:0">Next <span uk-icon="icon:chevron-right; ratio:1.5"></span></a>
									</div>
									
						        </div>
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 " style="
								    position: absolute;
								    margin-bottom: 0;
								    bottom: 0;
								    width:100%;">
								    <div class="uk-width-1-1 width-100">
										<p style="font-size:8px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut </p>
									</div>
								</div>
						    </div>
						</div>

						<div id="modal-group-7" class="uk-transition-fade" uk-modal uk-position-cover>
							<!--div class="cohete-modal" style="
								position: absolute;left:-6em;z-index:99">
								<img style="height:89vh;" src="./img/design/cohete.png">
							</div-->
						    <div class="uk-modal-dialog padding-top-60"
						    style="height:80vh; width:80vw">
						        <button class="uk-modal-close-default" type="button" uk-close></button>
						        <div class="uk-modal-body">
						        	<div class="uk-flex uk-flex-center">
						        		<div style="">4/6</div>
						        	</div>
						        	<p class="title padding-top-60" style="">
						        		¿Cuantos comensales atiendes o atenderás en tu punto de venta?
						        	</p>
						        	<!----las opciones---->
									<div class="uk-container uk-container-small aling-center">
										<div uk-grid class="uk-flex uk-flex-center uk-text-center" style="margin-left:0">
											<div class="uk-width-1-3@m tenemos"
											style="border-right:solid 1px #ff7a0b">
												<a class="container-tenemos" href="#">
													<div class="uk-flex uk-flex-center">
														<div class="uk-flex uk-flex-middle uk-flex-center transicion circle">
															De 100 a 250
														</div>
													</div>
												</a>
											</div>
											<div class="uk-width-1-3@m tenemos"
											style="border-right:solid 1px #ff7a0b">
												<a class="container-tenemos" href="#">
													<div class="uk-flex uk-flex-center">
														<div class="uk-flex uk-flex-middle uk-flex-center transicion circle">
															De 250 a 500
														</div>
													</div>
												</a>
											</div>
											<div class="uk-width-1-3@m tenemos">
												<a class="container-tenemos" href="#">
													<div class="uk-flex uk-flex-center">
														<div class="uk-flex uk-flex-middle uk-flex-center transicion circle">
															Mas de 500
														</div>
													</div>
												</a>
											</div>
										</div>
										<div uk-grid class="uk-text-center margin-left-0">
											
										</div>
									</div>

						        </div>
						        <!------footer modal--->
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
								    position: absolute;
								    margin-bottom: 30px;
								    bottom: 0;
								    width:100%;">
						        	<!------botones--->
									<div class="uk-flex uk-flex-center uk-align-left cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-6" class="uk-align-left cero" uk-toggle style="margin-left:0"><span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo</a>
									</div>
									<div class="uk-width-expand@m">
										<div class="uk-flex uk-flex-center width-100">
										    <img src="./img/design/logo-footer.png">
										</div>
									</div>
									<div class="uk-flex uk-flex-center uk-align-right cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-8" class="uk-align-right cero" uk-toggle style="margin-left:0">Next <span uk-icon="icon:chevron-right; ratio:1.5"></span></a>
									</div>
									
						        </div>
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 " style="
								    position: absolute;
								    margin-bottom: 0;
								    bottom: 0;
								    width:100%;">
								    <div class="uk-width-1-1 width-100">
										<p style="font-size:8px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut </p>
									</div>
								</div>
						    </div>
						</div>

						<div id="modal-group-8" class="uk-transition-fade" uk-modal uk-position-cover>
							<!--div class="cohete-modal" style="
								position: absolute;left:-6em;z-index:99">
								<img style="height:89vh;" src="./img/design/cohete.png">
							</div-->
						    <div class="uk-modal-dialog padding-top-60"
						    style="height:80vh; width:80vw">
						        <button class="uk-modal-close-default" type="button" uk-close></button>
						        <div class="uk-modal-body">
						        	<div class="uk-flex uk-flex-center">
						        		<div style="">4/6</div>
						        	</div>
						        	<p class="title padding-top-60" style="">
						        		¿A traves de que medio te gustaría que te contactaramos?
						        	</p>
						        	<!----las opciones---->
									<div class="uk-container uk-container-small aling-center">
										<div uk-grid class="uk-flex uk-flex-center uk-text-center" style="margin-left:0">
											<form class="uk-container">
											    <fieldset class="uk-fieldset">
											        <div class="uk-margin uk-grid-small uk-child-width-auto aling-center uk-grid" uk-grid>
											            <label class="cero uk-width-1-1 uk-flex uk-flex-center">
											            	<input class="uk-radio margin-top-5" type="radio" name="radio2" checked> Teléfono</label>
											            <label class="cero uk-width-1-1 uk-flex uk-flex-center">
											            	<input class="uk-radio" type="radio" name="radio2"> E-mail</label>
											            <label class="cero uk-width-1-1 uk-flex uk-flex-center">
											            	<input class="uk-radio" type="radio" name="radio2"> Celular</label>
											            <label class="cero uk-width-1-1 uk-flex uk-flex-center">
											            	<input class="uk-radio" type="radio" name="radio2"> Watsapp</label>
											        </div>
											        
											        <button class="uk-button uk-button-default boton margin-top-50">Siguiente</button>
											    </fieldset>
											</form>
										</div>

										<div uk-grid class="uk-text-center margin-left-0 margin-right-10">
											
										</div>
									</div>

						        </div>
						        <!------footer modal--->
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
								    position: absolute;
								    margin-bottom: 30px;
								    bottom: 0;
								    width:100%;">
						        	<!------botones--->
									<div class="uk-flex uk-flex-center uk-align-left cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-7" class="uk-align-left cero" uk-toggle style="margin-left:0"><span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo</a>
									</div>
									<div class="uk-width-expand@m">
										<div class="uk-flex uk-flex-center width-100">
										    <img src="./img/design/logo-footer.png">
										</div>
									</div>
									<div class="uk-flex uk-flex-center uk-align-right cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-9" class="uk-align-right cero" uk-toggle style="margin-left:0">Next <span uk-icon="icon:chevron-right; ratio:1.5"></span></a>
									</div>
									
						        </div>
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 " style="
								    position: absolute;
								    margin-bottom: 0;
								    bottom: 0;
								    width:100%;">
								    <div class="uk-width-1-1 width-100">
										<p style="font-size:8px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut </p>
									</div>
								</div>
						    </div>
						</div>
						
						<div id="modal-group-9" class="uk-transition-fade" uk-modal uk-position-cover>
							<!--div class="cohete-modal" style="
								position: absolute;left:-6em;z-index:99">
								<img style="height:89vh;" src="./img/design/cohete.png">
							</div-->
						    <div class="uk-modal-dialog padding-top-60"
						    style="height:80vh; width:80vw">
						        <button class="uk-modal-close-default" type="button" uk-close></button>
						        <div class="uk-modal-body">
						        	<div class="uk-flex uk-flex-center">
						        		<div style="">5/6</div>
						        	</div>
						        	<p class="title padding-top-60" style="">
						        		Escribe la info <br>para contactarte
						        	</p>
						        	<!----las opciones---->
									<div class="uk-container uk-container-small aling-center">
										<div uk-grid class="uk-flex uk-flex-center uk-text-center" style="margin-left:0">
											<form class="uk-container">
											    <fieldset class="uk-fieldset">
													<div class="uk-margin uk-flex uk-flex-center" uk-grid>
														<div style="border: solid #ff7a0b 2px;border-radius: 20px; width:600px" >
												    		<input class="cero uk-input aling-center" type="text" placeholder="Valdermoro, escribenos tu correo electronico" style="border: solid transparent;width:90%; float: left; width: 94%; ">
												    		<span>
												    		<a href="#modal-group-2" class="cero" uk-toggle>
												    			<span class="cero" style="background-color:#ff7a0b;
																    color: #fff;
																    border-radius: 100%;
																    padding:7px;
																    position: absolute;
																    z-index: 99;
																    margin-left: -23px;
																    margin-top: -1px;
																    " uk-icon="icon:mail; ratio:1.4"></span>
															</a>
												    		</span>
												    	</div>
													</div>
													<div class="uk-margin uk-flex uk-flex-center" uk-grid>
														<div style="border: solid #ff7a0b 2px;border-radius: 20px; width:600px" >
												    		<input class="cero uk-input aling-center" type="text" placeholder="¿Cual es tu número?" style="border: solid transparent;width:90%; float: left; width: 94%; ">
												    		<span>
												    		<a href="#modal-group-2" class="cero" uk-toggle>
												    			<span class="cero" style="background-color:#ff7a0b;
																    color: #fff;
																    border-radius: 100%;
																    padding:7px;
																    position: absolute;
																    z-index: 99;
																    margin-left: -23px;
																    margin-top: -1px;
																    " uk-icon="icon:  receiver; ratio:1.4"></span>
															</a>
												    		</span>
												    	</div>
													</div>
											        
											        <button class="uk-button uk-button-default boton margin-top-50">Siguiente</button>
											    </fieldset>
											</form>
										</div>
										
										<div uk-grid class="uk-text-center margin-left-0 margin-right-10">
											
										</div>
									</div>

						        </div>
						        <!------footer modal--->
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
								    position: absolute;
								    margin-bottom: 30px;
								    bottom: 0;
								    width:100%;">
						        	<!------botones--->
									<div class="uk-flex uk-flex-center uk-align-left cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-8" class="uk-align-left cero" uk-toggle style="margin-left:0"><span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo</a>
									</div>
									<div class="uk-width-expand@m">
										<div class="uk-flex uk-flex-center width-100">
										    <img src="./img/design/logo-footer.png">
										</div>
									</div>
									<div class="uk-flex uk-flex-center uk-align-right cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-10" class="uk-align-right cero" uk-toggle style="margin-left:0">Next <span uk-icon="icon:chevron-right; ratio:1.5"></span></a>
									</div>
									
						        </div>
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 " style="
								    position: absolute;
								    margin-bottom: 0;
								    bottom: 0;
								    width:100%;">
								    <div class="uk-width-1-1 width-100">
										<p style="font-size:8px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut </p>
									</div>
								</div>
						    </div>
						</div>
						
						<div id="modal-group-10" class="uk-transition-fade" uk-modal uk-position-cover>
							<!--div class="cohete-modal" style="
								position: absolute;left:-6em;z-index:99">
								<img style="height:89vh;" src="./img/design/cohete.png">
							</div-->
						    <div class="uk-modal-dialog padding-top-60"
						    style="height:80vh; width:80vw">
						        <button class="uk-modal-close-default" type="button" uk-close></button>
						        <div class="uk-modal-body">
						        	<div class="uk-flex uk-flex-center">
						        		<div style="">6/6</div>
						        	</div>
						        	<p class="title padding-top-60" style="font-size:46px; font-weight:800">
						        		Hasta pronto, Valdomero
						        	</p>
						        	
						        	<div class="title-2 padding-top-40">
						        		Pronto estaremos en contacto contigo.
						        	</div>
									<div class="uk-margin uk-flex uk-flex-center" uk-grid>
										<div style="width:600px" >
									    		<p class="uk-text-center">
									    			Gracias por viajar con Spacefood.
									    		</p>
									    </div>
									</div>
						        </div>
						        <!------footer modal--->
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
								    position: absolute;
								    margin-bottom: 30px;
								    bottom: 0;
								    width:100%;">
						        	<!------botones--->
									<div class="uk-flex uk-flex-center uk-align-left cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-9" class="uk-align-left cero" uk-toggle style="margin-left:0"><span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo</a>
									</div>
									<div class="uk-width-expand@m">
										<div class="uk-flex uk-flex-center width-100">
										    <img src="./img/design/logo-footer.png">
										</div>
									</div>
									<div class="uk-flex uk-flex-center uk-align-right cero"
									style="
									height:30px;
									width:120px;
									margin-top:30px;">
										<a href="#modal-group-11" class="uk-align-right cero" uk-toggle style="margin-left:0">Next <span uk-icon="icon:chevron-right; ratio:1.5"></span></a>
									</div>
									
						        </div>
						        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 " style="
								    position: absolute;
								    margin-bottom: 0;
								    bottom: 0;
								    width:100%;">
								    <div class="uk-width-1-1 width-100">
										<p style="font-size:8px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut </p>
									</div>
								</div>
						    </div>
						</div>

				<!-- =============================================== -->
				<!-- =============================================== -->

			</div>
		    <div class="padding-top-10">
		    	<div uk-grid  class="margin-left-0 cero margin-top-menos-20" style="margin-top:-5px">
				    <div class="uk-text-center margin-left-0 number">1</div>
				    <div class="uk-width-expand padding-top-30">
				    	Cuentanos,
						<h6 class="font-bold padding-bottom-10">¿Qué tienes en mente?</h6>
				    </div>
				</div>
				<div uk-grid  class="margin-left-0 cero margin-top-menos-20" style="margin-top:-5px">
				    <div class="uk-text-center margin-left-0 number">2</div>
				    <div class="uk-width-expand padding-top-30">
				    	Cuentanos,
						<h6 class="font-bold padding-bottom-10">¿Qué tienes en mente?</h6>
				    </div>
				</div>
				<div uk-grid  class="margin-left-0 cero margin-top-menos-20" style="margin-top:-5px">
				    <div class="uk-text-center margin-left-0 number cero">3</div>
				    <div class="uk-width-expand padding-top-30">
				    	Cuentanos,
						<h6 class="font-bold padding-bottom-10">¿Qué tienes en mente?</h6>
				    </div>
				</div>
		    </div>
		</div>
	</section>