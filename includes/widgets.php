<?php

$modal='
<!-- =============  MODAL FORMULARIO =============== -->
<!-- =============================================== -->
    <div id="modal-group-1" class="uk-transition-fade" uk-modal uk-position-cover uk-grid>
        <!--div class="cohete-modal" style="
            position: absolute;left:-6em;z-index:99">
            <img style="height:89vh;" src="./img/design/cohete.png">
        </div-->
        <div class="uk-modal-dialog uk-margin uk-flex uk-flex-center uk-flex-middle" 
        style="height:90vh; padding-left:0; width:90vw">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-body cero margin-left-0 width-100" style="height:95%;padding-bottom:30px;">
                <p class="title uk-text-center" style="font-size:46px; font-weight:800">
                    Hagamos un plan
                </p>
                <div class="title-2">¿Estás listo?</div>
                <div class="title padding-top-100 padding-bottom-10">¿Cuál es tu nombre?</div>
                <div class="uk-flex uk-flex-center margin-left-0  " uk-grid>
                    <div style="border: solid #ff7a0b 2px;border-radius: 20px; width:60%" >
                        <input class="cero uk-input aling-center" type="text" placeholder="Escribe tu nombre aquí" style="border: solid transparent;width:90%; float: left; width: 94%; ">
                        <span>
                            <a href="#modal-group-2" class="cero" uk-toggle>
                                <span class="cero" style="background-color:#ff7a0b;
                                color: #fff;
                                border-radius: 100%;
                                width: 42px;
                                height: 42px;
                                position: absolute;
                                z-index: 99;
                                margin-right: 20px;" uk-icon="icon:chevron-right; ratio: 2"></span>
                            </a>
                        </span>
                    </div>
                </div>
            </div>

            <!------footer modal--->
            <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
            position: absolute;
            margin-top:30px;
            margin-bottom:0;
            bottom: 0;
            width:100%;">
            <!------botones--->
                <div class="uk-flex uk-flex-center uk-align-left cero"
                style="
                height:30px;
                width:120px;
                margin-top:30px;">
                    <a href="#modal-group-1" class="uk-align-left cero" uk-toggle style="margin-left:0">
                        <span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo
                    </a>
                </div>
                <div class="uk-width-expand@m">
                    <div class="uk-flex uk-flex-center width-100">
                        <img style="padding-bottom: 5px;" src="./img/design/logo-footer.png">
                    </div>
                </div>
                <div class="uk-flex uk-flex-center uk-align-right cero"
                style="
                height:30px;
                width:120px;
                margin-top:30px;">
                </div>
            </div>
            <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
            position: absolute;
            margin-bottom: 0;
            bottom: 0;
            width:100%;">
                <div class="uk-width-1-1 cero width-100">
                    <p class="cero" style="font-size:10px">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut </p>
                </div>
            </div>
            
        </div>
    </div>

    <div id="modal-group-2" class="uk-transition-fade" uk-modal uk-position-cover>
        <!--div class="cohete-modal" style="
            position: absolute;left:-6em;z-index:99">
            <img style="height:89vh;" src="./img/design/cohete.png">
        </div-->
        <div class="uk-modal-dialog uk-margin uk-flex uk-flex-center uk-flex-middle" 
        style="height:90vh; padding-left:0; width:90vw">
        <button class="uk-modal-close-default" type="button" uk-close></button>
        <div class="uk-modal-body cero margin-left-0 width-100" style="height:auto;padding-bottom:30px;">
            <p class="title padding-top-60" style="font-size:46px; font-weight:800">
                Hola, Valdomero
            </p>

            <div class="title-2 padding-top-40">en Spacefood queremos ser parte de tu órbita.</div>
            <div class="uk-margin uk-flex uk-flex-center" uk-grid>
                <div style="width:600px" >
                    <p class="uk-text-center">
                        Tu viaje en Spacefood comienza aquí.
                    </p>
                </div>
            </div>
        </div>
        <!------footer modal--->
        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
        position: absolute;
        margin-bottom: 30px;
        bottom: 0;
        width:100%;">
            <!------botones--->
            <div class="uk-flex uk-flex-center uk-align-left cero"
            style="
            height:30px;
            width:120px;
            margin-top:30px;">
            <a href="#modal-group-1" class="uk-align-left cero" uk-toggle style="margin-left:0">
                <span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo
            </a>
            </div>
            <div class="uk-width-expand@m">
                <div class="uk-flex uk-flex-center width-100">
                    <img style="padding-bottom: 5px;" src="./img/design/logo-footer.png">
                </div>
            </div>
            <div class="uk-flex uk-flex-center uk-align-right cero"
            style="
            height:30px;
            width:120px;
            margin-top:30px;">
            <a href="#modal-group-3" class="uk-align-right cero" uk-toggle style="margin-left:0">
                Next <span uk-icon="icon:chevron-right; ratio:1.5"></span>
            </a>
            </div>

        </div>
        <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
        position: absolute;
        margin-bottom: 0;
        bottom: 0;
        width:100%;">
            <div class="uk-width-1-1 cero width-100">
                <p class="cero" style="font-size:10px">
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut 
                </p>
            </div>
            </div>
        </div>
    </div>

    <div id="modal-group-3" class="uk-transition-fade" uk-modal uk-position-cover>
        <!--div class="cohete-modal" style="
            position: absolute;left:-6em;z-index:99">
            <img style="height:89vh;" src="./img/design/cohete.png">
        </div-->
        <div class="uk-modal-dialog uk-margin uk-flex uk-flex-center uk-flex-middle" 
        style="height:90vh; padding-left:0; width:90vw">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-body cero margin-left-0 width-100" style="height:auto;padding-bottom:30px;">
                <div class="cero uk-flex uk-flex-center">
                    <div style="
                    border-radius:100%; 
                    height:50px;
                    width:50px;
                    background-color:#ff7a0b"></div>
                </div>
                <p class="cero uk-text-center padding-top-60">
                    Esta es tu capsula de viaje, da click en las opciones de abajo para empezar
                </p>
                <p class="title padding-top-60" style="">
                    Selecciona la opción que estás buscando
                </p>
                <!----las opciones---->
                <div class="uk-container uk-container-small aling-center">
                    <div uk-grid class="uk-text-center" style="margin-left:0">
                        <div class="uk-width-1-2@s uk-width-1-4@m tenemos"
                        style="border-right:solid 1px #ff7a0b">
                            <a class="container-tenemos" href="#">
                                <div class="uk-flex-center">
                                    <div class="img-tenemos transicion tenemos-1" style="height:120px;width:120px"></div>
                                </div>
                                <h6 class="font-bold">Consumibles</h6>
                            </a>
                        </div>
                        <div class="uk-width-1-2@s uk-width-1-4@m tenemos"
                        style="border-right:solid 1px #ff7a0b">
                            <a class="container-tenemos" href="#">
                                <div class="uk-flex-center">
                                    <div class="img-tenemos transicion tenemos-2" style="height:120px;width:120px"></div>
                                </div>
                                <h6 class="font-bold">Venta de equipo</h6>
                            </a>
                        </div>
                        <div class="uk-width-1-2@s uk-width-1-4@m tenemos"
                        style="border-right:solid 1px #ff7a0b">
                            <a class="container-tenemos" href="#">
                                <div class="uk-flex-center">
                                    <div class="img-tenemos transicion tenemos-3" style="height:120px;width:120px"></div>
                                </div>
                                <h6 class="font-bold">Venta de equipo</h6>
                            </a>
                        </div>
                        <div class="uk-width-1-2@s uk-width-1-4@m tenemos">
                            <a class="container-tenemos" href="#">
                                <div class="uk-flex-center">
                                    <div class="img-tenemos transicion tenemos-4" style="height:120px;width:120px"></div>
                                </div>
                                <h6 class="font-bold">Venta de equipo</h6>
                            </a>
                        </div>
                    </div>
                    <div uk-grid class="uk-text-center margin-left-0">

                    </div>
                </div>

            </div>

            <!------footer modal--->
            <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
            position: absolute;
            margin-bottom: 30px;
            bottom: 0;
            width:100%;">
            <!------botones--->
                <div class="uk-flex uk-flex-center uk-align-left cero"
                style="
                height:30px;
                width:120px;
                margin-top:30px;">
                    <a href="#modal-group-2" class="uk-align-left cero" uk-toggle style="margin-left:0">
                        <span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo
                    </a>
                </div>
                <div class="uk-width-expand@m">
                    <div class="uk-flex uk-flex-center width-100">
                        <img style="padding-bottom: 5px;" src="./img/design/logo-footer.png">
                    </div>
                </div>
                <div class="uk-flex uk-flex-center uk-align-right cero"
                style="
                height:30px;
                width:120px;
                margin-top:30px;">
                    <a href="#modal-group-4" class="uk-align-right cero" uk-toggle style="margin-left:0">
                        Next <span uk-icon="icon:chevron-right; ratio:1.5"></span>
                    </a>
                </div>

            </div>
            <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
            position: absolute;
            margin-bottom: 0;
            bottom: 0;
            width:100%;">
                <div class="uk-width-1-1 cero width-100">
                    <p class="cero" style="font-size:10px">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut 
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-group-4" class="uk-transition-fade" uk-modal uk-position-cover>
        <!--div class="cohete-modal" style="
            position: absolute;left:-6em;z-index:99">
            <img style="height:89vh;" src="./img/design/cohete.png">
        </div-->
        <div class="uk-modal-dialog uk-margin uk-flex uk-flex-center uk-flex-middle" 
        style="height:90vh; padding-left:0; width:90vw">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-body cero margin-left-0 width-100" style="height:auto;padding-bottom:30px;">
                <div class="uk-flex uk-flex-center">
                    <div style="">1/6</div>
                </div>
                <div class="uk-flex uk-flex-center uk-text-center">
                    <img style="" src="./img/design/grafica-1.png">
                </div>
                <p class="title padding-top-60" style="">
                    ¿Para cuantos necesitas el equipo?
                </p>
                <!----las opciones---->
                <div class="uk-container uk-container-small aling-center">
                    <div uk-grid class="uk-flex uk-flex-center uk-text-center" style="margin-left:0">
                        <div class="uk-width-1-2@s uk-width-1-4@m tenemos"
                        style="border-right:solid 1px #ff7a0b">
                            <a class="container-tenemos" href="#">
                                <div class="uk-flex uk-flex-center">
                                    <div class="uk-flex uk-flex-middle uk-flex-center transicion circle">
                                        Inmediato
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="uk-width-1-2@s uk-width-1-4@m tenemos"
                        style="border-right:solid 1px #ff7a0b">
                            <a class="container-tenemos" href="#">
                                <div class="uk-flex uk-flex-center">
                                    <div class="uk-flex uk-flex-middle uk-flex-center transicion circle">
                                        En 3 meses o menos
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="uk-width-1-2@s uk-width-1-4@m tenemos"
                        style="border-right:solid 1px #ff7a0b">
                            <a class="container-tenemos" href="#">
                                <div class="uk-flex uk-flex-center">
                                    <div class="uk-flex uk-flex-middle uk-flex-center transicion circle">
                                        Entre 3 y 6 meses
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="uk-width-1-2@s uk-width-1-4@m tenemos">
                            <a class="container-tenemos" href="#">
                                <div class="uk-flex uk-flex-center">
                                    <div class="uk-flex uk-flex-middle uk-flex-center transicion circle">
                                        Entre 6 meses y un año
                                    </div>
                                </div>
                            </a>
                        </div>

                    </div>
                    <div uk-grid class="uk-text-center margin-left-0">

                    </div>
                </div>

             </div>
             <!------footer modal--->
            <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
            position: absolute;
            margin-bottom: 30px;
            bottom: 0;
            width:100%;">
                <!------botones--->
                <div class="uk-flex uk-flex-center uk-align-left cero"
                style="
                height:30px;
                width:120px;
                margin-top:30px;">
                    <a href="#modal-group-3" class="uk-align-left cero" uk-toggle style="margin-left:0">
                        <span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo
                    </a>
                </div>
                <div class="uk-width-expand@m">
                    <div class="uk-flex uk-flex-center width-100">
                        <img style="padding-bottom: 5px;" src="./img/design/logo-footer.png">
                    </div>
                </div>
                <div class="uk-flex uk-flex-center uk-align-right cero"
                style="
                height:30px;
                width:120px;
                margin-top:30px;">
                    <a href="#modal-group-5" class="uk-align-right cero" uk-toggle style="margin-left:0">
                        Next <span uk-icon="icon:chevron-right; ratio:1.5"></span>
                    </a>
                </div>

            </div>
            <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
            position: absolute;
            margin-bottom: 0;
            bottom: 0;
            width:100%;">
                <div class="uk-width-1-1 cero width-100">
                    <p class="cero" style="font-size:10px">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut 
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-group-5" class="uk-transition-fade" uk-modal uk-position-cover>
        <!--div class="cohete-modal" style="
        position: absolute;left:-6em;z-index:99">
        <img style="height:89vh;" src="./img/design/cohete.png">
        </div-->
        <div class="uk-modal-dialog uk-margin uk-flex uk-flex-center uk-flex-middle" 
        style="height:90vh; padding-left:0; width:90vw">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-body cero margin-left-0 width-100" style="height:auto;padding-bottom:30px;">
                <div class="uk-flex uk-flex-center">
                    <div style="">2/6</div>
                </div>
                <div class="uk-flex uk-flex-center uk-text-center">
                    <img style="" src="./img/design/grafica-2.png">
                </div>
                <!----las opciones---->
                <div class="uk-container uk-container-small aling-center">
                    <div uk-grid class="uk-text-center" style="margin-left:0">
                        <div class="uk-width-1-3@m tenemos"
                        style="border-right:solid 1px #ff7a0b">
                            <a class="container-tenemos" href="#">
                                <div class="uk-flex-center">
                                    <div class="img-tenemos transicion tenemos-1"  style="height:120px;width:120px"></div>
                                </div>
                                <h6 class="font-bold">Nuevo</h6>
                                <p class="cero" style="font-size:12px">Estoy iniciando mi primer punto de venta desde cero. </p>
                            </a>
                        </div>
                        <div class="uk-width-1-3@m tenemos"
                        style="border-right:solid 1px #ff7a0b">
                            <a class="container-tenemos" href="#">
                                <div class="uk-flex-center">
                                    <div class="img-tenemos transicion tenemos-2"  style="height:120px;width:120px"></div>
                                </div>
                                <h6 class="font-bold">Crecimiento</h6>
                                <p class="cero" style="font-size:12px">Quiero abrir una nueva sucursal para tener de 2 a 5 puntos de venta.</p>
                            </a>
                        </div>
                        <div class="uk-width-1-3@m tenemos">
                            <a class="container-tenemos" href="#">
                                <div class="uk-flex-center">
                                    <div class="img-tenemos transicion tenemos-4"  style="height:120px;width:120px"></div>
                                </div>
                                <h6 class="font-bold">Exploción</h6>
                                <p class="cero" style="font-size:12px">Ya tengo más de 5 sucursales operando y queremos abrir más puntos de venta.</p>
                            </a>
                        </div>
                    </div>
                    <div uk-grid class="uk-text-center margin-left-0">

                    </div>
                </div>

            </div>
            <!------footer modal--->
            <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
            position: absolute;
            margin-bottom: 30px;
            bottom: 0;
            width:100%;">
                <!------botones--->
                <div class="uk-flex uk-flex-center uk-align-left cero"
                style="
                height:30px;
                width:120px;
                margin-top:30px;">
                    <a href="#modal-group-4" class="uk-align-left cero" uk-toggle style="margin-left:0">
                        <span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo
                    </a>
                </div>
                <div class="uk-width-expand@m">
                    <div class="uk-flex uk-flex-center width-100">
                        <img style="padding-bottom: 5px;" src="./img/design/logo-footer.png">
                    </div>
                </div>
                <div class="uk-flex uk-flex-center uk-align-right cero"
                style="
                height:30px;
                width:120px;
                margin-top:30px;">
                    <a href="#modal-group-6" class="uk-align-right cero" uk-toggle style="margin-left:0">
                        Next <span uk-icon="icon:chevron-right; ratio:1.5"></span>
                    </a>
                </div>

            </div>
            <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
            position: absolute;
            margin-bottom: 0;
            bottom: 0;
            width:100%;">
                <div class="uk-width-1-1 cero width-100">
                    <p class="cero" style="font-size:10px">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut 
                    </p>
                </div>
            </div>

        </div>
    </div>

    <div id="modal-group-6" class="uk-transition-fade" uk-modal uk-position-cover>
        <!--div class="cohete-modal" style="
        position: absolute;left:-6em;z-index:99">
        <img style="height:89vh;" src="./img/design/cohete.png">
        </div-->
         <div class="uk-modal-dialog uk-margin uk-flex uk-flex-center uk-flex-middle" 
        style="height:90vh; padding-left:0; width:90vw">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-body cero margin-left-0 width-100" style="height:auto;padding-bottom:30px;">
                <div class="uk-flex uk-flex-center">
                    <div style="">3/6</div>
                </div>
                <div class="uk-flex uk-flex-center uk-text-center">
                    <img style="" src="./img/design/grafica-3.png">
                </div>
                <p class="title padding-top-60" style="">
                    ¿Cuál equipo estás buscando?
                </p>
                <!----las opciones---->
                <div class="uk-flex uk-flex-center aling-center cero" style="height:160px;">
                    <div uk-grid class="uk-flex uk-flex-center uk-text-center cero" style="margin-left:0; height:auto;">
                        <div class="uk-container aling-center cero" id="menu2" style="height:100px;">
                            <div uk-grid class="uk-text-center cero" style="margin-left:0">
                                <div class="uk-width-1-5@m item-container cero" style="height:100;">
                                    <a class="container-item cero">
                                        <div class="uk-flex-center">
                                            <div class="img-element transicion element-1" style="height:100px;width:120px"></div>
                                            <h6 class="font-bold cero">Horno de pizza</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="uk-width-1-5@m item-container">
                                    <a class="container-item">
                                        <div class="uk-flex-center">
                                            <div class="img-element transicion element-1" style="height:100px;width:120px"></div>
                                            <h6 class="font-bold cero">Maquinas de hielo</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="uk-width-1-5@m item-container">
                                    <a class="container-item">
                                        <div class="uk-flex-center">
                                            <div class="img-element transicion element-1" style="height:100px;width:120px"></div>
                                            <h6 class="font-bold cero">Freidora</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="uk-width-1-5@m item-container">
                                    <a class="container-item">
                                        <div class="uk-flex-center">
                                            <div class="img-element transicion element-1" style="height:100px;width:120px"></div>
                                            <h6 class="font-bold cero">Maquinas de helados</h6>
                                        </div>
                                    </a>
                                </div>
                                <div class="uk-width-1-5@m item-container">
                                    <a class="container-item">
                                        <div class="uk-flex-center">
                                            <div class="img-element transicion element-1" style="height:100px;width:120px"></div>
                                            <h6 class="font-bold cero">Freidora</h6>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div uk-grid class="uk-text-center margin-left-0">

                            </div>
                        </div>

                    </div>
                </div>
                <!----input---->
                <div class="uk-margin uk-flex uk-flex-center cero" uk-grid>
                    <p class="title cero" style="">
                    ¿Otro?</p>
                </div>
                <div class="uk-margin uk-flex uk-flex-center cero" uk-grid>
                    <div style="border: solid #ff7a0b 2px;border-radius: 20px; width:600px" >
                        <input class="cero uk-input aling-center" type="text" placeholder="Describelo aquí" style="border: solid transparent;width:90%; float: left; width: 94%; ">
                        <span>
                            <a href="#modal-group-2" class="cero" uk-toggle>
                                <span class="cero" style="background-color:#ff7a0b;
                                color: #fff;
                                border-radius: 100%;
                                width: 42px;
                                height: 42px;
                                position: absolute;
                                z-index: 99;
                                margin-right: 20px;" uk-icon="icon:chevron-right; ratio: 2"></span>
                            </a>
                        </span>
                    </div>
                </div>
            
                <!------footer modal--->
                <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
                position: absolute;
                margin-bottom: 30px;
                bottom: 0;
                width:100%;">
                    <!------botones--->
                    <div class="uk-flex uk-flex-center uk-align-left cero"
                    style="
                    height:30px;
                    width:120px;
                    margin-top:30px;">
                        <a href="#modal-group-5" class="uk-align-left cero" uk-toggle style="margin-left:0">
                            <span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo
                        </a>
                    </div>
                    <div class="uk-width-expand@m">
                        <div class="uk-flex uk-flex-center width-100">
                            <img style="padding-bottom: 5px;" src="./img/design/logo-footer.png">
                        </div>
                    </div>
                    <div class="uk-flex uk-flex-center uk-align-right cero"
                    style="
                    height:30px;
                    width:120px;
                    margin-top:30px;">
                        <a href="#modal-group-7" class="uk-align-right cero" uk-toggle style="margin-left:0">
                            Next <span uk-icon="icon:chevron-right; ratio:1.5"></span>
                        </a>
                    </div>

                </div>
                <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
                position: absolute;
                margin-bottom: 0;
                bottom: 0;
                width:100%;">
                    <div class="uk-width-1-1 cero width-100">
                        <p class="cero" style="font-size:10px">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut 
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-group-7" class="uk-transition-fade" uk-modal uk-position-cover>
        <!--div class="cohete-modal" style="
        position: absolute;left:-6em;z-index:99">
        <img style="height:89vh;" src="./img/design/cohete.png">
        </div-->
        <div class="uk-modal-dialog uk-margin uk-flex uk-flex-center uk-flex-middle" 
        style="height:90vh; padding-left:0; width:90vw">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-body cero margin-left-0 width-100" style="height:auto;padding-bottom:30px;">
                <div class="uk-flex uk-flex-center">
                    <div style="">4/6</div>
                </div>
                <div class="uk-flex uk-flex-center uk-text-center">
                    <img style="" src="./img/design/grafica-4.png">
                </div>
                <p class="title padding-top-60" style="">
                    ¿Cuantos comensales atiendes o atenderás en tu punto de venta?
                </p>
                <!----las opciones---->
                <div class="uk-container uk-container-small aling-center">
                    <div uk-grid class="uk-flex uk-flex-center uk-text-center" style="margin-left:0">
                        <div class="uk-width-1-3@m tenemos"
                        style="border-right:solid 1px #ff7a0b">
                            <a class="container-tenemos" href="#">
                                <div class="uk-flex uk-flex-center">
                                    <div class="uk-flex uk-flex-middle uk-flex-center transicion circle">
                                        De 100 a 250
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="uk-width-1-3@m tenemos"
                        style="border-right:solid 1px #ff7a0b">
                            <a class="container-tenemos" href="#">
                                <div class="uk-flex uk-flex-center">
                                    <div class="uk-flex uk-flex-middle uk-flex-center transicion circle">
                                        De 250 a 500
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="uk-width-1-3@m tenemos">
                            <a class="container-tenemos" href="#">
                                <div class="uk-flex uk-flex-center">
                                    <div class="uk-flex uk-flex-middle uk-flex-center transicion circle">
                                        Mas de 500
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div uk-grid class="uk-text-center margin-left-0">

                    </div>
                </div>

            </div>
            <!------footer modal--->
                <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
                position: absolute;
                margin-bottom: 30px;
                bottom: 0;
                width:100%;">
                    <!------botones--->
                    <div class="uk-flex uk-flex-center uk-align-left cero"
                    style="
                    height:30px;
                    width:120px;
                    margin-top:30px;">
                        <a href="#modal-group-6" class="uk-align-left cero" uk-toggle style="margin-left:0">
                            <span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo
                        </a>
                    </div>
                    <div class="uk-width-expand@m">
                        <div class="uk-flex uk-flex-center width-100">
                            <img style="padding-bottom: 5px;" src="./img/design/logo-footer.png">
                        </div>
                    </div>
                    <div class="uk-flex uk-flex-center uk-align-right cero"
                    style="
                    height:30px;
                    width:120px;
                    margin-top:30px;">
                        <a href="#modal-group-8" class="uk-align-right cero" uk-toggle style="margin-left:0">
                            Next <span uk-icon="icon:chevron-right; ratio:1.5"></span>
                        </a>
                    </div>

                </div>
                <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
                position: absolute;
                margin-bottom: 0;
                bottom: 0;
                width:100%;">
                    <div class="uk-width-1-1 cero width-100">
                        <p class="cero" style="font-size:10px">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut 
                        </p>
                    </div>
                </div>
        </div>
    </div>

    <div id="modal-group-8" class="uk-transition-fade" uk-modal uk-position-cover>
        <!--div class="cohete-modal" style="
        position: absolute;left:-6em;z-index:99">
        <img style="height:89vh;" src="./img/design/cohete.png">
        </div-->
       <div class="uk-modal-dialog uk-margin uk-flex uk-flex-center uk-flex-middle" 
        style="height:90vh; padding-left:0; width:90vw">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-body cero margin-left-0 width-100" style="height:auto;padding-bottom:30px;">
                <div class="uk-flex uk-flex-center">
                    <div style="">4/6</div>
                </div>
                <div class="uk-flex uk-flex-center uk-text-center">
                    <img style="" src="./img/design/grafica-5.png">
                </div>
                <p class="title padding-top-60" style="">
                    ¿A traves de que medio te gustaría que te contactaramos?
                </p>
                <!----las opciones---->
                <div class="uk-container uk-container-small aling-center">
                    <div uk-grid class="uk-flex uk-flex-center uk-text-center" style="margin-left:0">
                        <form class="uk-container">
                            <fieldset class="uk-fieldset">
                                <div class="uk-margin uk-grid-small uk-child-width-auto aling-center uk-grid" uk-grid>
                                    <label class="cero uk-width-1-1 uk-flex uk-flex-center">
                                        <input class="uk-radio margin-top-5" type="radio" name="radio2" checked> Teléfono
                                    </label>
                                    <label class="cero uk-width-1-1 uk-flex uk-flex-center">
                                            <input class="uk-radio" type="radio" name="radio2"> E-mail
                                    </label>
                                    <label class="cero uk-width-1-1 uk-flex uk-flex-center">
                                        <input class="uk-radio" type="radio" name="radio2"> Celular
                                    </label>
                                    <label class="cero uk-width-1-1 uk-flex uk-flex-center">
                                        <input class="uk-radio" type="radio" name="radio2"> Watsapp
                                    </label>
                                </div>

                                <button class="uk-button uk-button-default boton margin-top-10">Siguiente</button>
                            </fieldset>
                        </form>
                    </div>

                    <div uk-grid class="uk-text-center margin-left-0 margin-right-10">

                    </div>
                </div>

            </div>
            
            <!------footer modal--->
            <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
            position: absolute;
            margin-bottom: 30px;
            bottom: 0;
            width:100%;">
                <!------botones--->
                <div class="uk-flex uk-flex-center uk-align-left cero"
                style="
                height:30px;
                width:120px;
                margin-top:30px;">
                    <a href="#modal-group-7" class="uk-align-left cero" uk-toggle style="margin-left:0">
                        <span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo
                    </a>
                </div>
                <div class="uk-width-expand@m">
                    <div class="uk-flex uk-flex-center width-100">
                        <img style="padding-bottom: 5px;" src="./img/design/logo-footer.png">
                    </div>
                </div>
                <div class="uk-flex uk-flex-center uk-align-right cero"
                style="
                height:30px;
                width:120px;
                margin-top:30px;">
                    <a href="#modal-group-9" class="uk-align-right cero" uk-toggle style="margin-left:0">
                        Next <span uk-icon="icon:chevron-right; ratio:1.5"></span>
                    </a>
                </div>

            </div>
            <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
            position: absolute;
            margin-bottom: 0;
            bottom: 0;
            width:100%;">
                <div class="uk-width-1-1 cero width-100">
                    <p class="cero" style="font-size:10px">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut 
                    </p>
                </div>
            </div>
               
        </div>
    </div>

    <div id="modal-group-9" class="uk-transition-fade" uk-modal uk-position-cover>
        <!--div class="cohete-modal" style="
        position: absolute;left:-6em;z-index:99">
        <img style="height:89vh;" src="./img/design/cohete.png">
        </div-->
        <div class="uk-modal-dialog uk-margin uk-flex uk-flex-center uk-flex-middle" 
        style="height:90vh; padding-left:0; width:90vw">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-body cero margin-left-0 width-100" style="height:auto;padding-bottom:30px;">
                <div class="uk-flex uk-flex-center">
                    <div style="">5/6</div>
                </div>
                <div class="uk-flex uk-flex-center uk-text-center">
                    <img style="" src="./img/design/grafica-6.png">
                </div>
                <p class="title padding-top-60" style="">
                    Escribe la info <br>para contactarte
                </p>
                <!----las opciones---->
                <div class="uk-container uk-container-small aling-center">
                    <div uk-grid class="uk-flex uk-flex-center uk-text-center" style="margin-left:0">
                        <form class="uk-container">
                            <fieldset class="uk-fieldset">
                                <div class="uk-margin uk-flex uk-flex-center" uk-grid>
                                    <div style="border: solid #ff7a0b 2px;border-radius: 20px; width:600px" >
                                        <input class="cero uk-input aling-center" type="text" placeholder="Valdermoro, escribenos tu correo electronico" style="border: solid transparent;width:90%; float: left; width: 94%; ">
                                        <span>
                                            <a href="#modal-group-2" class="cero" uk-toggle>
                                                <span class="cero" style="background-color:#ff7a0b;
                                                color: #fff;
                                                border-radius: 100%;
                                                padding:7px;
                                                position: absolute;
                                                z-index: 99;
                                                margin-left: -23px;
                                                margin-top: -1px;
                                                " uk-icon="icon:mail; ratio:1.4"></span>
                                            </a>
                                        </span>
                                    </div>
                                </div>
                                <div class="uk-margin uk-flex uk-flex-center" uk-grid>
                                    <div style="border: solid #ff7a0b 2px;border-radius: 20px; width:600px" >
                                        <input class="cero uk-input aling-center" type="text" placeholder="¿Cual es tu número?" style="border: solid transparent;width:90%; float: left; width: 94%; ">
                                        <span>
                                            <a href="#modal-group-2" class="cero" uk-toggle>
                                                <span class="cero" style="background-color:#ff7a0b;
                                                color: #fff;
                                                border-radius: 100%;
                                                padding:7px;
                                                position: absolute;
                                                z-index: 99;
                                                margin-left: -23px;
                                                margin-top: -1px;
                                                " uk-icon="icon:  receiver; ratio:1.4"></span>
                                            </a>
                                        </span>
                                    </div>
                                </div>

                                <button class="uk-button uk-button-default boton margin-top-50">Siguiente</button>
                            </fieldset>
                        </form>
                    </div>

                    <div uk-grid class="uk-text-center margin-left-0 margin-right-10">

                    </div>
                </div>

            </div>
            <!------footer modal--->
            <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
            position: absolute;
            margin-bottom: 30px;
            bottom: 0;
            width:100%;">
                <!------botones--->
                <div class="uk-flex uk-flex-center uk-align-left cero"
                style="
                height:30px;
                width:120px;
                margin-top:30px;">
                    <a href="#modal-group-8" class="uk-align-left cero" uk-toggle style="margin-left:0">
                        <span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo
                    </a>
                </div>
                <div class="uk-width-expand@m">
                    <div class="uk-flex uk-flex-center width-100">
                        <img style="padding-bottom: 5px;" src="./img/design/logo-footer.png">
                    </div>
                </div>
                <div class="uk-flex uk-flex-center uk-align-right cero"
                style="
                height:30px;
                width:120px;
                margin-top:30px;">
                    <a href="#modal-group-10" class="uk-align-right cero" uk-toggle style="margin-left:0">
                        Next <span uk-icon="icon:chevron-right; ratio:1.5"></span>
                    </a>
                </div>

            </div>
            <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
            position: absolute;
            margin-bottom: 0;
            bottom: 0;
            width:100%;">
                <div class="uk-width-1-1 cero width-100">
                    <p class="cero" style="font-size:10px">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut 
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-group-10" class="uk-transition-fade" uk-modal uk-position-cover>
        <!--div class="cohete-modal" style="
        position: absolute;left:-6em;z-index:99">
        <img style="height:89vh;" src="./img/design/cohete.png">
        </div-->
        <div class="uk-modal-dialog uk-margin uk-flex uk-flex-center uk-flex-middle" 
            style="height:90vh; padding-left:0; width:90vw">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-body cero margin-left-0 width-100" style="height:auto;padding-bottom:30px;">
                <div class="uk-flex uk-flex-center">
                    <div style="">6/6</div>
                </div>
                <p class="title padding-top-60" style="font-size:46px; font-weight:800">
                    Hasta pronto, Valdomero
                </p>

                <div class="title-2 padding-top-40">
                    Pronto estaremos en contacto contigo.
                </div>
                <div class="uk-margin uk-flex uk-flex-center" uk-grid>
                    <div style="width:600px" >
                        <p class="uk-text-center">
                            Gracias por viajar con Spacefood.
                        </p>
                    </div>
                </div>
            </div>
            <!------footer modal--->
            <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
            position: absolute;
            margin-bottom: 30px;
            bottom: 0;
            width:100%;">
                <!------botones--->
                <div class="uk-flex uk-flex-center uk-align-left cero"
                style="
                height:30px;
                width:120px;
                margin-top:30px;">
                    <a href="#modal-group-9" class="uk-align-left cero" uk-toggle style="margin-left:0">
                        <span uk-icon="icon:chevron-left; ratio:1.5"></span>Previo
                    </a>
                </div>
                <div class="uk-width-expand@m">
                    <div class="uk-flex uk-flex-center width-100">
                        <img style="padding-bottom: 5px;" src="./img/design/logo-footer.png">
                    </div>
                </div>
                <div class="uk-flex uk-flex-center uk-align-right cero"
                style="
                height:30px;
                width:120px;
                margin-top:30px;">
                    <a href="#modal-group-10" class="uk-align-right cero" uk-toggle style="margin-left:0">
                        Next <span uk-icon="icon:chevron-right; ratio:1.5"></span>
                    </a>
                </div>

            </div>
            <div class="uk-flex uk-flex-center uk-container-expand aling-center width-100 cero" style="
            position: absolute;
            margin-bottom: 0;
            bottom: 0;
            width:100%;">
                <div class="uk-width-1-1 cero width-100">
                    <p class="cero" style="font-size:10px">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut 
                    </p>
                </div>
            </div>
        </div>
    </div>

<!-- =============================================== -->';
$hagamosUnPlan='
<!--PLAN-->
<section class="uk-container padding-70-0" id="plan">
    <div class="cohete" style="
    position: absolute;left:0;">
    <img style="
    height: 480px;
    float:left;
    margin:0;
    padding:0;
    left:-50px;" src="./img/design/cohete.png">
</div>
<div class="uk-grid-divider uk-child-width-expand@s margin-left-0 padding-30-0" uk-grid>
    <div class="aling-center">
        <h1 class="title padding-top-20 padding-bottom-20">
        Hagamos un plan</h1>
        <h6 class="font-bold padding-bottom-10">¡Recibe asesoría gratuita!</h6>
        Si estás buscando abrir un restaurante, una nueva sucursal o simplemente necesitas ampliar tu capacidad de producción, nosotros podemos ayudarte a llegar hasta el espacio sideral.

        <button class="uk-button uk-button-default button-border margin-top-30" type="button" uk-toggle="target: #modal-group-1"> ¡Comencemos! </button>

        <!-- =============================================== -->
        '.$modal.'
        <!-- =============================================== -->

    </div>
    <div class="padding-top-10">
        <div uk-grid  class="margin-left-0 cero margin-top-menos-20" style="margin-top:-5px">
            <div class="uk-text-center margin-left-0 number">1</div>
            <div class="uk-width-expand padding-top-30">
                Cuentanos,
                <h6 class="font-bold padding-bottom-10">¿Qué tienes en mente?</h6>
            </div>
        </div>
        <div uk-grid  class="margin-left-0 cero margin-top-menos-20" style="margin-top:-5px">
            <div class="uk-text-center margin-left-0 number">2</div>
            <div class="uk-width-expand padding-top-30">
                Cuentanos,
                <h6 class="font-bold padding-bottom-10">¿Qué tienes en mente?</h6>
            </div>
        </div>
        <div uk-grid  class="margin-left-0 cero margin-top-menos-20" style="margin-top:-5px">
            <div class="uk-text-center margin-left-0 number cero">3</div>
            <div class="uk-width-expand padding-top-30">
                Cuentanos,
                <h6 class="font-bold padding-bottom-10">¿Qué tienes en mente?</h6>
            </div>
        </div>
    </div>
</div>
</section>
';

$buscador='
    <!--BUSCAR-->
    <div class="uk-width-expand@m">
        <div class="uk-flex uk-flex-center">
            <div class="uk-margin">
                <form class="uk-search uk-search-default">
                    <span class="uk-search-icon-flip padding-right-20" uk-search-icon></span>
                    <input class="uk-search-input uk-text-center" type="search" placeholder="Buscar...">
                </form>
            </div>
        </div>
    </div>
';

$menuCategorias='
<!--MENU CATEGORIAS tenemos todo lo necesario-->
    <!--MENU CATEGORIAS tenemos todo lo necesario-->
    <section  class="uk-visible@s" style="">
        <div class="uk-container aling-center" id="menu2">
            <h1 class="title margin-60-0 padding-top-60">Tenemos todo lo necesario</h1>
            <div uk-grid class="uk-text-center" style="margin-left:0">
                <div class="cero uk-width-1-5@m item-container">
                    <div class="container-item">
                        <div class="uk-flex-center">
                            <div class="img-element transicion element-1"></div>
                        </div>
                        <div class="contain-menu2 cero aling-center width-100 uk-flex uk-flex-center">
                            <h6 class="font-bold cero uk-text-truncate">Maquinas de hierro</h6>
                            <p class="cero">Categoria</p>
                            <hr class=""/>
                            <ul class="cero uk-text-truncate">
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="cero uk-width-1-5@m item-container">
                    <div class="container-item">
                        <div class="uk-flex-center">
                            <div class="img-element transicion element-1"></div>
                        </div>
                        <div class="contain-menu2 cero aling-center width-100 uk-flex uk-flex-center">
                            <h6 class="font-bold cero uk-text-truncate">Maquinas de hierro</h6>
                            <p class="cero">Categoria</p>
                            <hr class=""/>
                            <ul class="cero uk-text-truncate">
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="cero uk-width-1-5@m item-container">
                    <div class="container-item">
                        <div class="uk-flex-center">
                            <div class="img-element transicion element-1"></div>
                        </div>
                        <div class="contain-menu2 cero aling-center width-100 uk-flex uk-flex-center">
                            <h6 class="font-bold cero uk-text-truncate">Maquinas de hierro</h6>
                            <p class="cero">Categoria</p>
                            <hr class=""/>
                            <ul class="cero uk-text-truncate">
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="cero uk-width-1-5@m item-container">
                    <div class="container-item">
                        <div class="uk-flex-center">
                            <div class="img-element transicion element-1"></div>
                        </div>
                        <div class="contain-menu2 cero aling-center width-100 uk-flex uk-flex-center">
                            <h6 class="font-bold cero uk-text-truncate">Maquinas de hierro</h6>
                            <p class="cero">Categoria</p>
                            <hr class=""/>
                            <ul class="cero uk-text-truncate">
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="cero uk-width-1-5@m item-container">
                    <div class="container-item">
                        <div class="uk-flex-center">
                            <div class="img-element transicion element-1"></div>
                        </div>
                        <div class="contain-menu2 cero aling-center width-100 uk-flex uk-flex-center">
                            <h6 class="font-bold cero uk-text-truncate">Maquinas de hierro</h6>
                            <p class="cero">Categoria</p>
                            <hr class=""/>
                            <ul class="cero uk-text-truncate">
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero"> <a class="cero" href="sub-categoria">uno</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div uk-grid class="uk-text-center margin-left-0">
                
            </div>
        </div>
    </section>
    <!--SLIDER  movil-->
    <section class="aling-center margin-top-20 padding-30-0 uk-hidden@s"
    style="width:100%">
        <h1 class="uk-text-center title  padding-top-30">Tenemos todo lo necesario</h1>
        <div class="uk-position-relative uk-visible-toggle uk-dark aling-center" id="menu2" tabindex="-1" uk-slider style="">
            <ul class="uk-slider-items uk-child-width-1-1 aling-center" style="margin:0 2em; height:400px">
                <li>

                    <div class="uk-position-center uk-panel item-container" style="">
                    <div class="container-item">
                        <div class="uk-flex-center">
                            <div class="img-element transicion element-1"></div>
                        </div>
                        <div class="contain-menu2 cero aling-center width-100 uk-flex uk-flex-center">
                            <h6 class="font-bold cero uk-text-truncate">Maquinas de hierro</h6>
                            <p class="cero">Categoria</p>
                            <hr class=""/>
                            <ul class="cero ">
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                            </ul>
                        </div>
                    </div>
                    </div>
                </li>
                <li>
                    <div class="uk-position-center uk-panel item-container">
                    <div class="container-item">
                        <div class="uk-flex-center">
                            <div class="img-element transicion element-1"></div>
                        </div>
                        <div class="contain-menu2 cero aling-center width-100 uk-flex uk-flex-center">
                            <h6 class="font-bold cero">Maquinas de hierro</h6>
                            <p class="cero">Categoria</p>
                            <hr class=""/>
                            <ul class="cero ">
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                            </ul>
                        </div>
                    </div>
                    </div>
                </li>
                <li>
                    <div class="uk-position-center uk-panel item-container">
                    <div class="container-item">
                        <div class="uk-flex-center">
                            <div class="img-element transicion element-1"></div>
                        </div>
                        <div class="contain-menu2 cero aling-center width-100 uk-flex uk-flex-center">
                            <h6 class="font-bold cero">Maquinas de hierro</h6>
                            <p class="cero">Categoria</p>
                            <hr class=""/>
                            <ul class="cero ">
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                            </ul>
                        </div>
                    </div>
                    </div>
                </li>
                <li>
                    <div class="uk-position-center uk-panel item-container">
                    <div class="container-item">
                        <div class="uk-flex-center">
                            <div class="img-element transicion element-1"></div>
                        </div>
                        <div class="contain-menu2 cero aling-center width-100 uk-flex uk-flex-center">
                            <h6 class="font-bold cero">Maquinas de hierro</h6>
                            <p class="cero">Categoria</p>
                            <hr class=""/>
                            <ul class="cero ">
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                                <li class="cero uk-text-truncate"> <a class="cero" href="sub-categoria">uno</a></li>
                            </ul>
                        </div>
                    </div>
                    </div>
                </li>
               
            </ul>

            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
        </div>
    </section>
';


// CARRO DE COMPRA       
//unset($_SESSION['carro']);
if (isset($_POST['emptycart'])) {
unset($_SESSION['carro']);
}

$carroTotalProds=0;
// Si ya hay productos en la variable de sesión
if(isset($_SESSION['carro'])){
$arreglo=$_SESSION['carro'];
foreach ($arreglo as $key => $value) {
$carroTotalProds+=$value['Cantidad'];
}
}

// Remover artículos del carro
if (isset($_POST['removefromcart'])) {
$id=$_POST['id'];
$arregloAux=$_SESSION['carro'];
unset($arreglo);
$num=0;
foreach ($arregloAux as $key => $value) {
if ($id!=$value['Id']) {
$arreglo[]=array('Id'=>$arregloAux[$num]['Id'],'Cantidad'=>$arregloAux[$num]['Cantidad']);
}
$num++;
}
$_SESSION['carro']=$arreglo;
}

// Agregar artículos al carro
if (isset($_POST['addtocart'])) {
if (isset($_POST['cantidad']) and $_POST['cantidad']!==0 and $_POST['cantidad']!=='') {
$id=$_POST['id'];

$carroTotalProds+=$_POST['cantidad'];
$arregloNuevo[]=array('Id'=>$id,'Cantidad'=>$_POST['cantidad']);

if (!isset($arreglo)) {
$arreglo=$arregloNuevo;
}else{
$arregloAux=$arreglo;
unset($arreglo);
$num=0;
foreach ($arregloAux as $key => $value) {
if ($id!=$arregloAux[$num]['Id']) {
$arreglo[]=array('Id'=>$arregloAux[$num]['Id'],'Cantidad'=>$arregloAux[$num]['Cantidad']);
}else{
$carroTotalProds-=$arregloAux[$num]['Cantidad'];
}
$num++;
}
if ($_POST['cantidad']>0) {
$arreglo[]=array('Id'=>$id,'Cantidad'=>$_POST['cantidad']);
}
}

echo '{ "msg":"<div class=\'uk-text-center color-blanco bg-success padding-10 text-lg\'><i class=\'fa fa-check\'></i> &nbsp; Agregado al pedido</div>", "count":'.$carroTotalProds.' }';

$_SESSION['carro']=$arreglo;
}
}

if (isset($_POST['actualizarcarro'])) {
$arregloAux=$_SESSION['carro'];
unset($arreglo);
$carroTotalProds=0;
$num=0;
foreach ($arregloAux as $key => $value) {
$arreglo[]=array('Id'=>$arregloAux[$num]['Id'],'Cantidad'=>$_POST['cantidad'.$num]);
$carroTotalProds+=$_POST['cantidad'.$num];
$num++;
}
$_SESSION['carro']=$arreglo;
}

// LIMITAR PALABRAS      
function wordlimit($string, $length , $ellipsis)
{
    $words = explode(' ', strip_tags($string));
    if (count($words) > $length)
    {
        return implode(' ', array_slice($words, 0, $length)) ." ". $ellipsis;
    }
    else
    {
        return $string;
    }
}

// FECHA                 
// FECHA CORTA
function fechaCorta($fechaSQL){
$fechaSegundos=strtotime($fechaSQL);
$fechaY=date('Y',$fechaSegundos);
$fechaM=date('m',$fechaSegundos);
$fechaD=date('d',$fechaSegundos);
$fechaDay=strtolower(date('D',$fechaSegundos));

return $fechaD.'-'.$fechaM.'-'.$fechaY;
}

// FECHA Y HORA
function fechaHora($fechaSQL){
$fechaSegundos=strtotime($fechaSQL);
$fechaY=date('Y',$fechaSegundos);
$fechaM=date('m',$fechaSegundos);
$fechaD=date('d',$fechaSegundos);
$fechaH=date('H',$fechaSegundos);
$fechaI=date('i',$fechaSegundos);
$fechaDay=strtolower(date('D',$fechaSegundos));

return $fechaD.'-'.$fechaM.'-'.$fechaY.'<br>'.$fechaH.':'.$fechaI;
}

// SOLO HORA
function soloHora($fechaSQL){
$fechaSegundos=strtotime($fechaSQL);
$fechaH=date('H',$fechaSegundos);
$fechaI=date('i',$fechaSegundos);

return $fechaH.':'.$fechaI;
}

function fechaSQL($fechaSQL){
$fechaSegundos=strtotime($fechaSQL);

$fechaY=date('Y',$fechaSegundos);
$fechaM=date('m',$fechaSegundos);
$fechaD=date('d',$fechaSegundos);

return $fechaY.'/'.$fechaM.'/'.$fechaD;
}

// FECHA DIA
function fechaDisplayDia($fechaSQL){
$fechaSegundos=strtotime($fechaSQL);
$fechaY=date('Y',$fechaSegundos);
$fechaM=date('m',$fechaSegundos);
$fechaD=date('d',$fechaSegundos);
$fechaDay=strtolower(date('D',$fechaSegundos));

switch ($fechaDay) {
case 'mon':
$fechaDia='Lunes';
break;
case 'tue':
$fechaDia='Martes';
break;
case 'wed':
$fechaDia='Miércoles';
break;
case 'thu':
$fechaDia='Jueves';
break;
case 'fri':
$fechaDia='Viernes';
break;
case 'sat':
$fechaDia='Sábado';
break;
default:
$fechaDia='Domingo';
break;
}
return $fechaDia;
}

// FECHA MES
function fechaDisplayMes($fechaSQL){
$fechaSegundos=strtotime($fechaSQL);
$fechaY=date('Y',$fechaSegundos);
$fechaM=date('m',$fechaSegundos);
$fechaD=date('d',$fechaSegundos);
$fechaDay=strtolower(date('D',$fechaSegundos));

switch ($fechaM) {
case 1:
$mes='enero';
break;

case 2:
$mes='febrero';
break;

case 3:
$mes='marzo';
break;

case 4:
$mes='abril';
break;

case 5:
$mes='mayo';
break;

case 6:
$mes='junio';
break;

case 7:
$mes='julio';
break;

case 8:
$mes='agosto';
break;

case 9:
$mes='septiembre';
break;

case 10:
$mes='octubre';
break;

case 11:
$mes='noviembre';
break;

default:
$mes='diciembre';
break;
}

return $mes;
}

// FECHA LARGA
function fechaDisplay($fechaSQL){
$fechaSegundos=strtotime($fechaSQL);
$fechaY=date('Y',$fechaSegundos);
$fechaM=date('m',$fechaSegundos);
$fechaD=date('d',$fechaSegundos);
$fechaDay=strtolower(date('D',$fechaSegundos));

switch ($fechaM) {
case 1:
$mes='enero';
break;

case 2:
$mes='febrero';
break;

case 3:
$mes='marzo';
break;

case 4:
$mes='abril';
break;

case 5:
$mes='mayo';
break;

case 6:
$mes='junio';
break;

case 7:
$mes='julio';
break;

case 8:
$mes='agosto';
break;

case 9:
$mes='septiembre';
break;

case 10:
$mes='octubre';
break;

case 11:
$mes='noviembre';
break;

default:
$mes='diciembre';
break;
}

switch ($fechaDay) {
case 'mon':
$fechaDia='Lunes';
break;
case 'tue':
$fechaDia='Martes';
break;
case 'wed':
$fechaDia='Miércoles';
break;
case 'thu':
$fechaDia='Jueves';
break;
case 'fri':
$fechaDia='Viernes';
break;
case 'sat':
$fechaDia='Sábado';
break;
default:
$fechaDia='Domingo';
break;
}

return $fechaDia.' '.$fechaD.' de '.$mes.' de '.$fechaY;
}

// CARRUSEL              
// Carousel Inicio
function carousel($carousel){
global $CONEXION;
global $dominio;

$CONSULTA= $CONEXION -> query("SELECT * FROM configuracion WHERE id = 1");
$row_CONSULTA = $CONSULTA -> fetch_assoc();
switch ($row_CONSULTA['slideranim']) {
case 0:
$animation='fade';
break;
case 1:
$animation='slide';
break;
case 2:
$animation='scale';
break;
case 3:
$animation='pull';
break;
case 4:
$animation='push';
break;
default:
$animation='fade';
break;
}
$CAROUSEL = $CONEXION -> query("SELECT * FROM $carousel ORDER BY orden");
$numPics=$CAROUSEL->num_rows;
if ($numPics>0) {
echo '
<!-- Start Carousel -->
<div uk-slideshow="autoplay:true;ratio:'.$row_CONSULTA['sliderproporcion'].';animation:'.$animation.';min-height:'.$row_CONSULTA['sliderhmin'].';max-height:'.$row_CONSULTA['sliderhmax'].';" class="uk-grid-collapse" uk-grid>
    <div class="uk-visible-toggle uk-width-1-1 uk-flex-first">
        <div class="uk-position-relative">
            <ul class="uk-slideshow-items" style="width:100%;height:100vh!important;">';
                $num=0;
                $activo=' active';
                while ($row_CAROUSEL = $CAROUSEL -> fetch_assoc()) {
                $caption='';
                if (strlen($row_CAROUSEL['url'])>0) {
                        $pos=strpos($row_CAROUSEL['url'], $dominio);
                        $target=($pos>0)?'':'target="_blank"';
                        if ($row_CONSULTA['slidertextos']==1 AND strlen($row_CAROUSEL['titulo'])>0 AND strlen($row_CAROUSEL['url'])>0) {
                        $caption='
                            <div class="uk-position-bottom uk-transition-slide-bottom">
                                <div style="min-width:200px;min-height:100px;" class="uk-text-center">
                                    <a href="'.$row_CAROUSEL['url'].'" '.$target.' class="uk-button uk-button-white uk-button-large">
                                        '.$row_CAROUSEL['titulo'].'
                                    </a>
                                </div>
                            </div>';
                        }
                }
                echo '
                <li class="uk-flex uk-flex-center uk-flex-middle">
                    <img src="img/contenido/'.$carousel.'/'.$row_CAROUSEL['id'].'.jpg" uk-cover>
                    '.$caption.'
                    <div class="title uk-container-small" style="color:#fff;position: absolute;z-index:99;">
                        <p class="strong-text" style="padding:0 10%">'.$row_CAROUSEL['txt'].'</p></div>
                </li>';
                } echo '
            </ul>
            <div style="margin-left:auto;margin-right:auto">
                <div class="margin-top-10 uk-position-bottom-center" uk-grid>
                    <ul class="uk-slideshow-nav uk-dotnav uk-flex-right uk-margin"></ul>
                 </div>
            </div>
            <!--a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
            <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a-->
        </div>
    </div>
</div>
<!-- End Carousel -->
';
}
mysqli_free_result($CAROUSEL);
}




// ITEM                   
function item($id){
global $CONEXION;
global $caracteres_si_validos;
global $caracteres_no_validos;

$widget    = '';
$style     = 'max-width:200px;';  
$noPic     = 'img/design/camara.jpg';
$rutaPics  = 'img/contenido/productos/';
$firstPic  = $noPic;

$CONSULTA1 = $CONEXION -> query("SELECT * FROM productos WHERE id = $id");
$row_CONSULTA1 = $CONSULTA1 -> fetch_assoc();
$link=$id.'_'.urlencode(str_replace($caracteres_no_validos,$caracteres_si_validos,html_entity_decode(strtolower($row_CONSULTA1['titulo'])))).'-.html';

// Fotografía
$CONSULTA3 = $CONEXION -> query("SELECT * FROM productospic WHERE producto = $id ORDER BY orden,id LIMIT 1");
while ($rowCONSULTA3 = $CONSULTA3 -> fetch_assoc()) {
$firstPic = $rutaPics.$rowCONSULTA3['id'].'.jpg';
}

$picWidth=0;
$picHeight=0;
$picSize=getimagesize($firstPic);
foreach ($picSize as $key => $value) {
if ($key==3) {
$arrayCadena1=explode(' ',$value);
$arrayCadena1=str_replace('"', '', $arrayCadena1);
foreach ($arrayCadena1 as $key1 => $value1) {

$arrayCadena2=explode('=',$value1);
foreach ($arrayCadena2 as $key2 => $value2) {
if (is_numeric($value2)) {
$picProp[]=$value2;
}
}
}
}
}
if (isset($picProp)) {
$picWidth=$picProp[0];
$picHeight=$picProp[1];

$style=($picWidth<$picHeight)?'max-height:200px;':$style;
}

$widget.='
<div id="item'.$id.'" class="uk-text-center">
    <div class="bg-white padding-20" style="border:solid 1px #CCC;">
        <a href="'.$link.'" style="color:black;">
            <div class="margin-10">
                <div class="uk-flex uk-flex-center uk-flex-middle" style="height: 200px;">
                    <img data-src="'.$firstPic.'" uk-img style="'.$style.'">
                </div>
                <div style="min-height:100px;">
                    <div>
                        '.$row_CONSULTA1['sku'].'
                    </div>
                    <div class="uk-flex uk-flex-center">
                        <div class="line-yellow"></div>
                    </div>
                    <div class="padding-v-10">
                    </div>
                    <div>
                        '.$row_CONSULTA1['titulo'].'
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>';



return $widget;

}