<?php
/* %%%%%%%%%%%%%%%%%%%% MENSAJES               */
	if($mensaje!=''){
		$mensajes='
			<div class="uk-container">
				<div uk-grid>
					<div class="uk-width-1-1 margen-v-20">
						<div class="uk-alert-'.$mensajeClase.'" uk-alert>
							<a class="uk-alert-close" uk-close></a>
							'.$mensaje.'
						</div>					
					</div>
				</div>
			</div>';
	}

/* %%%%%%%%%%%%%%%%%%%% RUTAS AMIGABLES        */
		$rutaInicio			=	'Inicio';
		$rutaCategoria		=	'categoria';
		$rutaSubCategoria   =	'sub-categoria';
		$rutaProductos		=	'productos';
		$rutaDetalleProductos=	'detalle-productos';
		$rutaCasosExitosos		=	'casos-exitosos';
		$rutaPedido			=	$ruta.'Revisar_orden';

/* %%%%%%%%%%%%%%%%%%%% MENU                   */
	$menu='
		<li class="'.$nav1.'"><a href="'.$rutaInicio.'">Inicio</a></li>
		<li class="'.$nav2.'"><a href="'.$rutaCategoria.'">Categoria</a></li>
		<li class="'.$nav3.'"><a href="'.$rutaSubCategoria.'">subCategoria</a></li>
		<li class="'.$nav4.'"><a href="'.$rutaProductos.'">Productos</a></li>
		<li class="'.$nav5.'"><a href="'.$rutaDetalleProductos.'">DetalleProductos</a></li>
		<li class="'.$nav6.'"><a href="'.$rutaCasosExitosos.'">CasosDeExito</a></li>
		';

	$menuMovil='
		<li><a class="'.$nav1.'" href="'.$ruta.'">Inicio</a></li>
		<li><a class="'.$nav2.'" href="'.$rutaCategoria.'">Categoria</a></li>
		<li><a class="'.$nav3.'" href="'.$rutaSubCategoria.'">subCategoria</a></li>
		<li><a class="'.$nav4.'" href="'.$rutaProductos.'">Productos</a></li>
		<li><a class="'.$nav5.'" href="'.$rutaDetalleProductos.'">DetalleProductos</a></li>
		<li><a class="'.$nav6.'" href="'.$rutaCasosExitosos.'">CasosDeExito</a></li>
		';


/* %%%%%%%%%%%%%%%%%%%% HEADER                 */
	$header='
		<div class="uk-offcanvas-content uk-position-relative">
			<header  style="position: absolute;z-index:9; width:100%;">
				<div class="uk-container">
					<div uk-grid class="uk-grid-match margin-top-10">
						<a href="#menu-movil" uk-toggle id="menu" class="uk-icon-button uk-button-totop uk-box-shadow-large claro"><i class="fa fa-bars fa-1x" aria-hidden="true"></i>
							<br>
							<p class="cero" style="">MENU</p>
						</a>  
						<!--div class="uk-width-1-2@m uk-visible@s">
							<nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
								<div class="uk-navbar-center">
									<ul class="uk-navbar-nav">
										'.$menu.'
									</ul>
								</div>
							</nav>
						</div-->

						<!--div class="uk-width-1-4">
							'.$loginButton.'
						</div-->

					</div>
				</div>
			</header>

			'.$mensajes.'

			<!-- Off-canvas -->
			<div id="menu-movil" uk-offcanvas="mode: push;overlay: true" class="" style="">
				<section class="cero width-100 uk-flex uk-flex-center" style="background:transparent; position:absolute;z-index:999">
					<div class="uk-container uk-container-large aling-center cero uk-flex uk-flex-center" id="menu2">
						<div class="uk-text-center cero" uk-grid>
							<div class="uk-width-expand@m padding-top-60">
							       <div class="uk-flex uk-flex-center padding-top-10" style="padding-top:20px">
							        	<a href="'.$rutaInicio.'"> 
							        		<img class="cero" src="./img/design/logo-footer.png"
							        	style="width:180%;">
							        	</a>
								</div>
							</div>
						</div>
					</div>
				</section>
				<div class="uk-offcanvas-bar uk-flex uk-flex-column img1" style="
				    background: #f1f2f2;
				    height: 100vh;
				    width: 100%;
				    background-size: 100% 100%!important;
				    background-position: center;
				    background-image:linear-gradient(to bottom, rgba(241, 242, 242, 0.89) 0%,rgba(241, 242, 242, 0.89) 100%), url(./img/design/galaxia2.png);
				}"
				 style="">
					<button class="uk-offcanvas-close uk-dar" type="button" uk-close style="height:40px;width:40px;"></button>
					<div style="background:transparent;height:100vh;width:100%;" id="pc">
						<img class="" src="./img/design/estrellas.png" 
						style="position:absolute;
						margin-top: 50px;
						padding:20px;
						right:20px;
						width:240px">
						<div style="    
							height: auto;
						    margin: 0;
						    padding: 20% 0 6% 10%;
						    width: 80%;
						    margin-left: 20px;">
							<div class="whithoutlineBox">
								<div class="aling-center position-menu position-menu1"  uk-toggle="target: #modal-group-1">
									<a href="">SOLUCIONES</a>
								</div>
								<svg class="line" viewBox="00 00 100 10" preserveAspectRatio="none" >
								   <line x1="00" y1="00" x2="100" y2="10">
								</svg>
							</div>
							
							<div class="lineBox">
								<div class="aling-center position-menu position-menu2">
									<a href="">VISITA NUESTRO <br>SHOWROOM</a>
								</div>
								<svg class="line" viewBox="00 00 100 10" preserveAspectRatio="none" >
								   <line x1="00" y1="10" x2="100" y2="00">
								</svg>
							</div>
							<div class="whithoutlineBox">
								<div class="aling-center position-menu position-menu1"  uk-toggle="target: #modal-group-1">
									<a  href="Inicio">HAGANOS UN PLAN</a>
								</div>
								<svg class="line" viewBox="00 00 100 10" preserveAspectRatio="none" >
								   <line x1="00" y1="00" x2="100" y2="10">
								</svg>
							</div>
							<div class="lineBox">
								<div class="aling-center position-menu position-menu2">
									<a href="Inicio#spaceparts">SPACEPARTS<br>&nbsp;</a>
								</div>
								<svg class="line" viewBox="00 00 100 10" preserveAspectRatio="none" >
								   <line x1="00" y1="10" x2="100" y2="00">
								</svg>
							</div>
							<div class="whithoutlineBox">
								<div class="aling-center position-menu position-menu1">
									<a href="'.$rutaCategoria.'">MARCAS Y PRODUCTOS</a>
								</div>
								<svg class="line" viewBox="00 00 100 10" preserveAspectRatio="none" >
								   <line x1="00" y1="00" x2="100" y2="10">
								</svg>
							</div>
							<div class="lineBox-fin">
								<div class="aling-center position-menu position-menu3" style="position: absolute;z-index:99">
									<a href="">REGISTRARSE<br>&nbsp;</a></div>
							</div>
						</div>
						<img class="animated infinite pulse slower" src="./img/design/estrellas2.png" 
						style="position: absolute;
					    padding: 20px;
					    left: 120px;
					    width: 450px;
					    margin-top: -30px;">
					</div>	


					<!--MENUMOVIL-->
					<div style="background:transparent;height:auto;width:100%;" id="movil">
						<div style="height:auto;margin-top:90px;padding:40px;width:70%">	
						     <div class="lineBox-M-A">
						        <div class="position-movil-left">
						        </div>
						    </div>
						    <div class="lineBox-M-d">
						        <div class="position-movil-left">
						            <a href="">&nbsp;<br>SOLUCIONES</a>
						        </div>
						        <svg class="line-M" viewBox="00 00 100 10" preserveAspectRatio="none" >
						            <line x1="00" y1="00" x2="100" y2="10">
						        </svg>
						    </div>
						    <div class="lineBox-M" style="">
						        <div class="position-movil-right">
						            <a href="">VISITA NUESTRO <br>SHOWROOM</a>
						        </div>
						       <svg class="line-M" viewBox="00 00 100 10" preserveAspectRatio="none" >
						       <line x1="00" y1="10" x2="100" y2="00">
						       </svg>
						    </div>
						    <div class="lineBox-M-d" style="">
						        <div class="position-movil-left">
						            <a  href="Inicio">HAGANOS <br>UN PLAN</a>
						        </div>
						        <svg class="line-M" viewBox="00 00 100 10" preserveAspectRatio="none" >
						            <line x1="00" y1="00" x2="100" y2="10">
						        </svg>
						    </div>
						    <div class="lineBox-M" style="">
						        <div class="position-movil-right">
						            <a href="Inicio#spaceparts">&nbsp;<br>SPACEPARTS</a>
						        </div>
						      <svg class="line-M" viewBox="00 00 100 10" preserveAspectRatio="none" >
						       <line x1="00" y1="10" x2="100" y2="00">
						      </svg>
						    </div>
						    <div class="lineBox-M-d" style="">
						        <div class="position-movil-left">
						            <a href="'.$rutaCategoria.'">MARCAS Y<br>PRODUCTOS</a>
						        </div>
						        <svg class="line-M" viewBox="00 00 100 10" preserveAspectRatio="none" >
						            <line x1="00" y1="00" x2="100" y2="10">
						        </svg>
						    </div>
						    <div class="lineBox-M-B" style="">
						        <div class="position-movil-right">
						            <a href="">&nbsp;<br>REGISTRARSE</a></div>
						        </div>
						    </div>
						</div>
					</div>
					<!--terminaMENUMOVIL-->


				<div class="width-100 footer-menu">
					<div class="uk-flex uk-flex-center">
						<div class="uk-flex-center uk-text-center cero">
							<div class="cero uk-container ">
								<div class="uk-child-width-1-3 uk-text-center padding-0-60" uk-grid>
									<div class="">
										<span uk-icon="icon: facebook; ratio:2"></span>
									</div>
									<div class="">
										<span uk-icon="icon: twitter; ratio:2"></span>
									</div>
									<div class="">
										<span uk-icon="icon: youtube; ratio:2"></span>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>

				</div>
				

			</div>';

/* %%%%%%%%%%%%%%%%%%%% FOOTER                 */
	$stickerClass=($carroTotalProds==0 OR $identificador==500 OR $identificador==501 OR $identificador==502)?'uk-hidden':'';
	$footer = '
		<footer class="uk-flex-center">
				<div class="bg-footer" style="background:#f1f2f2;z-index: 0;">
					<div class="uk-container uk-position-relative">
						<div class="uk-width-1-1 uk-text-center">
							<div class="padding-v-50">
								<div class="uk-child-width-1-2@s uk-child-width-1-4@m uk-text-center" uk-grid>
								    <div>
								        <div class="uk-card uk-card-default uk-card-body">
								        	<img src="./img/design/footer-img.png"> 
								        </div>
								    </div>
								    <div>
								        <div class="uk-card uk-card-default uk-card-body">
								        	<img src="./img/design/footer-img.png"> 
								        </div>
								    </div>
								    <div>
								        <div class="uk-card uk-card-default uk-card-body">
								        	<img src="./img/design/footer-img.png"> 
								        </div>
								    </div>
								    <div>
								        <div class="uk-card uk-card-default uk-card-body">
								        	<img src="./img/design/footer-img.png"> 
								        </div>
								    </div>
								</div>
							</div>
							<div class="padding-v-20">
								<div class="uk-child-width-1-2@s uk-child-width-1-4@m cero uk-aling-center uk-text-center" uk-grid>
								    <div class="uk-grid-collapse uk-aling-center"  style="padding-left:0!important;">
								        <div class="uk-card uk-card-default uk-card-body ">
								        	<h6 class="cero font-bold uk-aling-center uk-text-center">OFICINAS CENTRALES <br>EN GUADALAJARA</h6>
								        	<hr class=" uk-flex uk-flex-center">
								        	<div class="uk-aling-center uk-text-center">
								        		Tel: 0133 3629 5044 <br>
												(con cinco líneas) <br>
												Fax: 0133 3673 2943 <br>
												Zona Occidente <br>
												Calzada Fresnos # 43-A <br>
												Col. Ciudad Granja C.P.45010 <br>
												Zapopan, Jalisco, México <br>
								        	</div>
								        </div>
								    </div>
								    <div class="uk-grid-collapse uk-aling-center">
								        <div class="uk-card uk-card-default uk-card-body ">
								        	<h6 class="cero font-bold uk-aling-center uk-text-center">OFICINAS DE LA <br>CIUDAD DE MÉXICO</h6>
								        	<hr class=" uk-flex uk-flex-center">
								        	<div class="uk-aling-center uk-text-center">
								        		Zona Centro<br>
												Tel: 0155 5682 0718<br>
												Fax:0155 5536 9442<br>
												Pestalozzi No. 851<br>
												Col. Del valle CP. 03020<br>
												Del. Benito Juárez,<br>
												México, D.F.
								        	</div>
								        </div>
								    </div>
								    <div class="uk-grid-collapse uk-aling-center">
								        <div class="uk-card uk-card-default uk-card-body ">
								        	<h6 class="cero font-bold uk-aling-center uk-text-center">OFICINAS EN <br> MONTERREY</h6>
								        	<hr class=" uk-flex uk-flex-center">
								        	<div class="uk-aling-center uk-text-center">
								        		Zona Norte<br>
												Tel: 0181 8357 6724<br>
												Fax: 0181 8357 6724<br>
												Puerto La Paz No. 4452<br>
												Col. Las brisas CP. 64780<br>
												Monterrey, <br>
												Nuevo León
								        	</div>
								        </div>
								    </div>
								    
								    <div class=" uk-flex uk-flex-center">
								        <div class="uk-card uk-card-default uk-card-body ">
								        	<h6 class="font-bold cero padding-top-8">EQUIPO</h6>
								        	<h6 class="uk-flex uk-flex-center font-bold cero padding-top-8">BOLSA DE TRABAJO</h6>
								        	<h6 class="font-bold cero padding-top-8">SPACEPARTS</h6>
								        	<h6 class="font-bold cero padding-top-8">OFERTAS</h6>
								        	<hr>
								        	<div class="uk-flex-center uk-text-center">
								        		<div class="uk-flex uk-flex-center">
								        			<h6 class="font-bold cero">SIGUENOS EN:</h6>
								        		</div>
								        		<div class="uk-container ">
									        		<div class="uk-child-width-1-2@s uk-child-width-1-3@m uk-text-center padding-0-60" uk-grid>
													    <div class="cero">
														    <span uk-icon="icon: facebook; ratio:2"></span>
													    </div>
													    <div class="cero">
													    	<span uk-icon="icon: twitter; ratio:2"></span>
													    </div>
													    <div class="cero">
													        <span uk-icon="icon: youtube; ratio:2"></span>
													    </div>
													</div>
												</div>
								        	</div>
								        </div>
								    </div>
								</div>
							</div>
							<div class="padding-v-20 uk-flex uk-flex-center">
								<div class="uk-text-center" uk-grid>
								    <div class=" uk-flex uk-flex-center margin-bottom-50">
								        <div class="uk-card uk-card-default uk-card-body ">
								        	<h6 class="font-bold cero text-xxl">spacefood <span uk-icon="icon:copyright; ratio: 2"></span>2019</h6>
								        	<hr class=" uk-flex uk-flex-center">
								        </div>
								    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</footer>
		<!--footer>
			<div class="bg-footer" style="z-index: 0;">
				<div class="uk-container uk-position-relative">
					<div class="uk-width-1-1 uk-text-center">
						<div class="padding-v-50">
							'.date('Y').' todos los derechos reservados Diseño por <a href="https://wozial.com/" target="_blank" class="color-negro">Wozial Marketing Lovers</a>
						</div>
					</div>
				</div>
			</div>
		</footer-->

		<div id="cotizacion-fixed" class="uk-position-top uk-height-viewport '.$stickerClass.'">
			<div>
				<a href="'.$rutaPedido.'"><img src="img/design/checkout.png"></a>
			</div>
		</div>

		'.$loginModal.'
    
		<div id="totop">
			<a href="#top" uk-scroll id="arrow-button" class="uk-icon-button uk-button-totop uk-box-shadow-large oscuro"><i class="fa fa-arrow-up fa-1x" aria-hidden="true"></i></a>
			<!--a href="#menu-movil" uk-toggle id="menu" class="uk-icon-button uk-button-totop uk-box-shadow-large claro"><i class="fa fa-bars fa-1x" aria-hidden="true"></i></a-->  
		</div>
	</div>';

/* %%%%%%%%%%%%%%%%%%%% HEAD GENERAL                */
	$headGNRL='
		<html lang="'.$languaje.'">
		<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">

			<meta charset="utf-8">
			<title>'.$title.'</title>
			<meta name="description" content="'.$description.'" />
			<meta property="fb:app_id" content="'.$appID.'" />
			<link rel="image_src" href="'.$ruta.$logoOg.'" />

			<meta property="og:type" content="website" />
			<meta property="og:title" content="'.$title.'" />
			<meta property="og:description" content="'.$description.'" />
			<meta property="og:url" content="'.$rutaEstaPagina.'" />
			<meta property="og:image" content="'.$ruta.$logoOg.'" />

			<meta itemprop="name" content="'.$title.'" />
			<meta itemprop="description" content="'.$description.'" />
			<meta itemprop="url" content="'.$rutaEstaPagina.'" />
			<meta itemprop="thumbnailUrl" content="'.$ruta.$logoOg.'" />
			<meta itemprop="image" content="'.$ruta.$logoOg.'" />

			<meta name="twitter:title" content="'.$title.'" />
			<meta name="twitter:description" content="'.$description.'" />
			<meta name="twitter:url" content="'.$rutaEstaPagina.'" />
			<meta name="twitter:image" content="'.$ruta.$logoOg.'" />
			<meta name="twitter:card" content="summary" />

			<meta name="viewport"       content="width=device-width, initial-scale=1">

			<link rel="icon"            href="'.$ruta.'img/design/favicon.ico" type="image/x-icon">
			<link rel="shortcut icon"   href="img/design/favicon.ico" type="image/x-icon">
			<link rel="stylesheet"      href="https://cdnjs.cloudflare.com/ajax/libs/uikit/'.$uikitVersion.'/css/uikit.min.css" />
			<link rel="stylesheet"      href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
			<link rel="stylesheet"      href="css/general.css">
			<link rel="stylesheet"      href="https://fonts.googleapis.com/css?family=Lato:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
			<link rel="stylesheet"      href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800&display=swap">

			<link rel="stylesheet"      href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">


			<!-- jQuery is required -->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

			<!-- UIkit JS -->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/'.$uikitVersion.'/js/uikit.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/'.$uikitVersion.'/js/uikit-icons.min.js"></script>
		</head>';


/* %%%%%%%%%%%%%%%%%%%% SCRIPTS                */
	$scriptGNRL='
		<script src="js/general.js"></script>

		<script>
			$(".cantidad").keyup(function() {
				var inventario = $(this).attr("data-inventario");
				var cantidad = $(this).val();
				inventario=1*inventario;
				cantidad=1*cantidad;
				if(inventario<=cantidad){
					$(this).val(inventario);
				}
				console.log(inventario+" - "+cantidad);
			})
			$(".cantidad").focusout(function() {
				var inventario = $(this).attr("data-inventario");
				var cantidad = $(this).val();
				inventario=1*inventario;
				cantidad=1*cantidad;
				if(inventario<=cantidad){
					//console.log(inventario*2+" - "+cantidad);
					$(this).val(inventario);
				}
			})

			// Agregar al carro
			$(".buybutton").click(function(){
				var id=$(this).data("id");
				var cantidad=$("#"+id).val();

				$.ajax({
					method: "POST",
					url: "addtocart",
					data: { 
						id: id,
						cantidad: cantidad,
						addtocart: 1
					}
				})
				.done(function( msg ) {
					datos = JSON.parse(msg);
					UIkit.notification.closeAll();
					UIkit.notification(datos.msg);
					$("#cartcount").html(datos.count);
					$("#cotizacion-fixed").removeClass("uk-hidden");
				});
			})
		</script>
		';



	// Script login Facebook
	$scriptGNRL.=(!isset($_SESSION['uid']) AND $dominio != 'localhost' AND isset($facebookLogin))?'
		<script>
			// Esta es la llamada a facebook FB.getLoginStatus()
			function statusChangeCallback(response) {
				if (response.status === "connected") {
					procesarLogin();
				} else {
					console.log("No se pudo identificar");
				}
			}

			// Verificar el estatus del login
			function checkLoginState() {
				FB.getLoginStatus(function(response) {
					statusChangeCallback(response);
				});
			}

			// Definir características de nuestra app
			window.fbAsyncInit = function() {
				FB.init({
					appId      : "'.$appID.'",
					xfbml      : true,
					version    : "v3.2"
				});
				FB.AppEvents.logPageView();
			};

			// Ejecutar el script
			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/es_LA/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, \'script\', \'facebook-jssdk\'));
			
			// Procesar Login
			function procesarLogin() {
				FB.api(\'/me?fields=id,name,email\', function(response) {
					console.log(response);
					$.ajax({
						method: "POST",
						url: "includes/acciones.php",
						data: { 
							facebooklogin: 1,
							nombre: response.name,
							email: response.email,
							id: response.id
						}
					})
					.done(function( response ) {
						console.log( response );
						datos = JSON.parse( response );
						UIkit.notification.closeAll();
						UIkit.notification(datos.msj);
						if(datos.estatus==0){
							location.reload();
						}
					});
				});
			}
		</script>

		':'';


// Reportar actividad
	$scriptGNRL.=(!isset($_SESSION['uid']))?'':'
		<script>
			var w;
			function startWorker() {
			  if(typeof(Worker) !== "undefined") {
			    if(typeof(w) == "undefined") {
			      w = new Worker("js/activityClientFront.js");
			    }
			    w.onmessage = function(event) {
					//console.log(event.data);
			    };
			  } else {
			    document.getElementById("result").innerHTML = "Por favor, utiliza un navegador moderno";
			  }
			}
			startWorker();
		</script>
		';

		

/* %%%%%%%%%%%%%%%%%%%% BUSQUEDA               */
	$scriptGNRL.='
		<script>
			$(document).ready(function(){
				$(".search").keyup(function(e){
					if(e.which==13){
						var consulta=$(this).val();
						var l = consulta.length;
						if(l>2){
							window.location = ("'.$ruta.'"+consulta+"_gdl");
						}else{
							UIkit.notification.closeAll();
							UIkit.notification("<div class=\'bg-danger color-blanco\'>Se requiren al menos 3 caracteres</div>");
						}
					}
				});
				$(".search-button").click(function(){
					var consulta=$(".search-bar-input").val();
					var l = consulta.length;
					if(l>2){
						window.location = ("'.$ruta.'"+consulta+"_gdl");
					}else{
						UIkit.notification.closeAll();
						UIkit.notification("<div class=\'bg-danger color-blanco\'>Se requiren al menos 3 caracteres</div>");
					}
				});
			});
		</script>';