<!DOCTYPE html>
<?=$headGNRL?>
	<section class="cero width-100" style="background:#000; position:absolute;z-index:990;">
		<div class="" style="margin-top:10px">
			<?=$header?>
		</div>
		<div class="aling-center cero uk-flex uk-flex-center width-100" id="menu2">
			<div class="uk-text-center cero" uk-grid>
				<div class="uk-width-expand@m">
			        <div class="uk-flex uk-flex-center margin-top-20">
			        	<img class="cero" src="./img/design/logo.png"
			        	style="width:80%">
					</div>
				</div>
			</div>
		</div>
	</section>

<body>

	
	<?=carousel('carousel')?>

	<!--HOVER TENEMOS-->
	<section class="uk-container aling-center margin-40-0 uk-visible@s">
		<h1 class="title padding-bottom-20">Tenemos todo lo necesario</h1>
		<div uk-grid class="uk-text-center" style="margin-left:0">
			<div class="uk-width-1-2@s uk-width-1-4@m tenemos">
				<a class="container-tenemos" href="#">
					<div class="uk-flex-center">
						<div class="img-tenemos transicion tenemos-1"></div>
					</div>
					<h6 class="font-bold">Venta de equipo</h6>
					<div>Tenemos los mejores equipos de las mejores marcas, para llevar tu cocina hasta las estrellas.</div>
				</a>
			</div>
			<div class="uk-width-1-2@s uk-width-1-4@m tenemos">
				<a class="container-tenemos" href="#">
					<div class="uk-flex-center">
						<div class="img-tenemos transicion tenemos-2"></div>
					</div>
					<h6 class="font-bold">Venta de equipo</h6>
					<div>Tenemos los mejores equipos de las mejores marcas, para llevar tu cocina hasta las estrellas.</div>
				</a>
			</div>
			<div class="uk-width-1-2@s uk-width-1-4@m tenemos">
				<a class="container-tenemos" href="#">
					<div class="uk-flex-center">
						<div class="img-tenemos transicion tenemos-3"></div>
					</div>
					<h6 class="font-bold">Venta de equipo</h6>
					<div>Tenemos los mejores equipos de las mejores marcas, para llevar tu cocina hasta las estrellas.</div>
				</a>
			</div>
			<div class="uk-width-1-2@s uk-width-1-4@m tenemos">
				<a class="container-tenemos" href="#">
					<div class="uk-flex-center">
						<div class="img-tenemos transicion tenemos-4"></div>
					</div>
					<h6 class="font-bold">Venta de equipo</h6>
					<div>Tenemos los mejores equipos de las mejores marcas, para llevar tu cocina hasta las estrellas.</div>
				</a>
			</div>
		</div>
		<div uk-grid class="uk-text-center margin-left-0">
			
		</div>
	</section>
	<!--TENEMOS movil-->
	<section class="aling-center margin-top-20 padding-30-0 uk-hidden@s"
	style="width:100%">
		<h1 class="uk-text-center title  padding-top-30">Tenemos todo lo necesario</h1>
		<div class="uk-position-relative uk-visible-toggle uk-dark aling-center" tabindex="-1" uk-slider style="">
		    <ul class="uk-slider-items uk-child-width-1-1 aling-center" style="margin:0 2em; height:340px">
		        <li>
		            <div class="uk-position-center uk-panel tenemos">
		            	<div class="uk-flex-center">
							<div class="img-tenemos transicion tenemos-1"></div>
						</div>
						<h6 class="font-bold">Venta de equipo</h6>
						<div>Tenemos los mejores equipos de las mejores marcas, para llevar tu cocina hasta las estrellas.</div>
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel tenemos">
		            	<div class="uk-flex-center">
							<div class="img-tenemos transicion tenemos-2"></div>
						</div>
						<h6 class="font-bold">Venta de equipo</h6>
						<div>Tenemos los mejores equipos de las mejores marcas, para llevar tu cocina hasta las estrellas.</div>
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel tenemos">
		            	<div class="uk-flex-center">
							<div class="img-tenemos transicion tenemos-3"></div>
						</div>
						<h6 class="font-bold">Venta de equipo</h6>
						<div>Tenemos los mejores equipos de las mejores marcas, para llevar tu cocina hasta las estrellas.</div>
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel tenemos">
		            	<div class="uk-flex-center">
							<div class="img-tenemos transicion tenemos-4"></div>
						</div>
						<h6 class="font-bold">Venta de equipo</h6>
						<div>Tenemos los mejores equipos de las mejores marcas, para llevar tu cocina hasta las estrellas.</div>
		            </div>
		        </li>
		    </ul>

		    <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
		    <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
		</div>
	</section>


	<!--MARCAS-->
	<section class="aling-center margin-top-20 padding-30-0"
	style="background:#f1f2f2; width:100%">
		<h1 class="uk-text-center title  padding-top-30">Marcas</h1>
		<div class="uk-position-relative uk-visible-toggle uk-dark aling-center" tabindex="-1" uk-slider style="height:150px">
		    <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@m aling-center" style="margin:0 2em">
		        <li uk-align-center style="padding-top:8em">
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-1.png">
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-2.png">
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-3.png">
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-4.png">
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-1.png">
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-2.png">
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-3.png">
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-4.png">
		            </div>
		        </li>
		    </ul>

		    <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
		    <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
		</div>
	</section>

	<!--PLAN-->
	<?=$hagamosUnPlan?>

	<!--CUADRADOS-->
	<section class="aling-center margin-left-0" id="banners">
		<div class="aling-center uk-text-center">
			<div class="uk-child-width-1-2@m uk-text-center uk-flex-center margin-left-0" uk-grid>
				<div class="margin-left-0 cero no-banner">
			        <div class="uk-card uk-width-auto@m uk-card-body cero">
			        	<div class="uk-flex-center">
							<div class="img-banner transicion banner-1"></div>
						</div>
				    </div>
			    </div>
			    <div class="margin-left-0 cero height-movil">
			        <div class="uk-card uk-width-auto@m uk-card-body si-banner-righ" uk-scrollspy="cls: uk-animation-slide-left; repeat: true" >
			        	<div class="aling-center padding-30">
					    	<h1 class="title">Visita nuestro Food Lab</h1>
							<p>
								Si estás en búsqueda de equipo nuevo para tu restaurante, agenda una visita a nuestro showroom y conoce los mejores equipos de esta galaxia.
								 <br><br>
								Estamos seguros que juntos encontraremos 
								la mejor opción.
							</p>
							<button class="uk-button uk-button-default button-border margin-top-30">Agenda una cita!</button>
						</div>
			        </div>
			    </div>
			    <div class="margin-left-0 cero uk-visible@m height-movil" id="spaceparts">
			        <div class="uk-card uk-width-auto@m uk-card-body cero si-banner-left height-movil" uk-scrollspy="cls: uk-animation-slide-right; repeat: true" >
			        	<div class="aling-center padding-30">
					    	<h1 class="title"> Spaceparts</h1>
							<p>Tenemos la selección más amplia de partes originales. 
								<br>
								Contamos con miles de piezas en stock con envío inmediato para que tu negocio nunca deje de volar.  
							</p>
							<ul>
								<li>Entregas a toda la república.</li>
								<li>Entregas inmediatas.</li>
								<li>Los precios mas bajos del mercado.</li>
								<li>Garantías y cobertura en todas las refacciones.</li>
							</ul>
							<button class="uk-button uk-button-default button-border margin-top-30">¡Comencemos!</button>
						</div>
			        </div>
			    </div>
			    <div class="margin-left-0 cero no-banner uk-visible@m">
			        <div class="uk-card uk-width-auto@m uk-card-body cero">
			        	<div class="uk-flex-center">
							<div class="img-banner transicion banner-1"></div>
						</div>
				    </div>
			    </div>
			    <!-- bloque para movil -->
			    <div class="margin-left-0 cero no-banner uk-hidden@m">
			        <div class="uk-card uk-width-auto@m uk-card-body cero">
			        	<div class="uk-flex-center">
							<div class="img-banner transicion banner-1"></div>
						</div>
				    </div>
			    </div>
			    <div class="margin-left-0 cero uk-hidden@m height-movil">
			        <div class="uk-card uk-width-auto@m uk-card-body si-banner-righ" uk-scrollspy="cls: uk-animation-slide-right; repeat: true" >
			        	<div class="aling-center padding-30">
					    	<h1 class="title"> Spaceparts</h1>
							<p>
								Tenemos la selección más amplia de partes originales.
								<br> 
								Contamos con miles de piezas en stock con envío inmediato para que tu negocio nunca deje de volar. 
							</p>
							<ul>
								<li>Entregas a toda la república.</li>
								<li>Entregas inmediatas.</li>
								<li>Los precios mas bajos del mercado.</li>
								<li>Garantías y cobertura en todas las refacciones.</li>
							</ul>
							<button class="uk-button uk-button-default button-border margin-top-30">¡Comencemos!</button>
						</div>
			        </div>
			    </div>
			
			    <div class="margin-left-0 cero no-banner">
			        <div class="uk-card uk-width-auto@m uk-card-body cero">
			        	<div class="uk-flex-center">
							<div class="img-banner transicion banner-1"></div>
						</div>
				    </div>
			    </div>
			    <div class="margin-left-0 cero height-movil">
			        <div class="uk-card uk-width-auto@m uk-card-body si-banner-righ" uk-scrollspy="cls: uk-animation-slide-left; repeat: true" >
			        	<div class="aling-center padding-30">
					    	<h1 class="title">
					    		Consumibles</h1>
							<p>
								La tecnología mas avanzada de la galaxia en consumibles alimenticios aterrizó con nosotros.
								<br><br>
								Obtén los mismos sabores y consistencias pero con ingredientes más eficientes.
							</p>
							<button class="uk-button uk-button-default button-border margin-top-30">Agenda una cita!</button>
						</div>
			        </div>
			    </div>
				
			</div>
		</div>
	</section>

	<!--SLIDER BANNER-->
	<section class="aling-center" id="slide2">
		<div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1" uk-slideshow="ratio: 7:3; animation: push">
		    <ul class="uk-slideshow-items">
		        <li> <div class="img1 picture-1"></div> </li>
		        <li> <div class="img1 picture-2"></div> </li>
		        <li> <div class="img1 picture-3"></div> </li>
		    </ul>
		    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"> </a>
		    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"> </a>
		    <button class="uk-button uk-button-default button-border margin-top-30">Cotizar</button>
		</div>
	</section>

	<!--MENU CATEGORIAS 'tenemos todo lo necesario'-->
	<?=$menuCategorias?>


	<!--SLIDER PRODUCTOS-->
	<section style="">
		<div class="uk-container aling-center top-slider" id="menu2">
			<div class="aling-center margin-top-20 padding-30-0" id="loUltimo"
			style="background:#fff; width:100%">
				<h1 class="uk-text-center title ">Maquina de Hielo</h1>
				<div class="uk-position-relative uk-visible-toggle uk-dark aling-center" tabindex="-1" uk-slider style="height:560px;">
				    <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@m aling-center" style="margin:0 2em; height:500px;">
				        <li uk-align-center>
				            <div class="uk-position-center uk-panel padding-20-0 item-container-top">
				            	<a class="cero" href="detalle-productos">
					            	<img src="./img/design/verMas.png">
					            	<h6> Manitowoc Ice Sotto UG 40 </h6>
					            	<p>
					            		Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
					            		
					            	</p>
					             </a>	
				            	<button class="uk-button uk-button-default button-border">Cotizar</button>
				            </div>
				        </li>
				        <li uk-align-center>
				            <div class="uk-position-center uk-panel padding-20-0  item-container-top">
				            	<a class="cero" href="detalle-productos">
					            	<img src="./img/design/verMas.png">
					            	<h6> Manitowoc Ice Sotto UG 40 </h6>
					            	<p>
					            		Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
					            		
					            	</p>
					            </a>
				            	<button class="uk-button uk-button-default button-border">Cotizar</button>
				            </div>
				        </li>
				        <li uk-align-center>
				            <div class="uk-position-center uk-panel padding-20-0  item-container-top">
				            	<a class="cero" href="detalle-productos">
					            	<img src="./img/design/verMas.png">
					            	<h6> Manitowoc Ice Sotto UG 40 </h6>
					            	<p>
					            		Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
					            		
					            	</p>
					            </a>
				            	<button class="uk-button uk-button-default button-border">Cotizar</button>
				            </div>
				        </li>
				        <li uk-align-center>
				            <div class="uk-position-center uk-panel padding-20-0  item-container-top">
				            	<a class="cero" href="detalle-productos">
				            		<img src="./img/design/verMas.png">
					            	<h6> Manitowoc Ice Sotto UG 40 </h6>
					            	<p>
					            		Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
					            	</p>
					            </a>
				            	<button class="uk-button uk-button-default button-border">Cotizar</button>
				            </div>
				        </li>
				    </ul>
				    <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
				    <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
				</div>
				<div class="padding-bottom-50">
					<a href="categoria" class="uk-button uk-button-default button-border">Ver más!</a>
				</div>
			</div>
		</div>
	</section>

	<!--CLIENTES SATISFECHOS-->
	<section class="aling-center margin-top-20 padding-30-0"
	style="background:#f1f2f2; width:100%">
		<h1 class="uk-text-center title  padding-top-30">Clientes satisfechos</h1>
		<div class="uk-position-relative uk-visible-toggle uk-dark aling-center" tabindex="-1" uk-slider style="height:150px">
		    <ul class="uk-slider-items uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@m aling-center" style="margin:0 2em">
		        <li uk-align-center style="padding-top:8em">
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-1.png">
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-2.png">
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-3.png">
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-4.png">
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-1.png">
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-2.png">
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-3.png">
		            </div>
		        </li>
		        <li>
		            <div class="uk-position-center uk-panel">
		            	<img src="./img/design/marca-home-4.png">
		            </div>
		        </li>
		    </ul>

		    <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
		    <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
		</div>
	</section>

	<!--CASOS DE EXITO-->
	<section id="exito">
		<div class="uk-child-width-1-2@s uk-grid-collapse uk-text-center" uk-grid>
		    <div class="padding-0-80 margin-top-80">
		    	<div class="title uk-text-left color-blanco" style="color:#f1f2f2">Roostería <br> <span> Rulas </span> </div>
		    	<div class="title-2 margin-top-30">Casos de  <span class="title-3"> éxito </span> </div>
		    	<div class="uk-flex margin-top-10">
		    		Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.
		    	</div>
		        <div class="img1 picture-3 padding-bottom-50">
		        	<a href="casos-exitosos" class="uk-button uk-button-default button-border margin-top-40">Ver más</a>
		        </div>
		    </div>
		    <div style="background-color:#f1f2f2">
		        <div class="width-100 vivencias" style="height:280px"> <div class="img"> </div> </div>
		        <div class="padding-0-80 margin-top-50">
		        	<h6 class="font-bold cero color-naranja text-xl">
		        		Roostería Rulas
		        	</h6>
		        	<h6 class="font-bold cero margin-top-25">
		        		Lorem ipsum dolor sit amet, consectetuer adipiscing elit
		        	</h6>
		        	<p class="margin-top-20">
		        		Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
		        	</p>
		        </div>
		    </div>
		</div>
	</section>

	<!--TRES BOTONES DE CONTACTO-->
	<section class="img1 necesito margin-bottom-80">
		<div class="uk-width-1-1 banner-title width-100" >
			Un pequeño formulario para un hombre
			<br/>
			<span class="banner-title banner-blond">Un gran salto para tu restaurante</span>
		</div>
		<div class="uk-width-1-1 padding-top-60 margin-bottom-80">
			<div class="uk-container uk-align-center">
				<div class="" uk-grid style="margin-left:0">
					<div class="uk-width-1-1@s uk-width-1-3@m uk-padding-remove " style="margin:0;margin-bottom:60px!important;">
					   <button class="uk-button uk-button-default boton-contactar">Necesito una<br>refacción</button>
					</div>
					<div class="uk-width-1-1@s uk-width-1-3@m uk-padding-remove margin-bottom-80" style="margin:0;margin-bottom:60px!important;">
					    <button class="uk-button uk-button-default boton-contactar ">Necesito equipo<br>para mi proyecto</button>
					</div>
					<div class="uk-width-1-1@s uk-width-1-3@m uk-padding-remove margin-bottom-80" style="margin:0;margin-bottom:60px!important;">
					   <button class="uk-button uk-button-default boton-contactar">Contactar un<br>asesor</button>
					</div>
				</div>
			</div>
		</div>
	</section>
			
	<?=$footer?>

	<?=$scriptGNRL?>

</body>
</html>