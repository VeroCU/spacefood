<!DOCTYPE html>
<?=$headGNRL?>
<body>
<?=$header?>
	<!--header-->
	<section class="uk-container uk-container-large aling-center" id="menu2">
		<div class="cero uk-text-center" uk-grid>
			<div class="cero uk-width-expand@m">
			       <div class="cero uk-flex uk-flex-center">
			        <a class="cero" href="Inicio">
				        <img class="logo-header"  src="./img/design/logo-footer.png">
				       </a>
				</div>
			</div>
		</div>
	</section>
	<!--banner-->
	<section class="banner-header img1 margin-bottom-50" style="height:180px">
	</section>

	<!--BUSCAR-->
    <section style="">
        <div class="uk-container uk-container-large aling-center" id="menu2">
            <div class="uk-text-center" uk-grid>
                <div class="uk-width-auto@m">
                    <div class="uk-flex uk-flex-center">
                    <a class="cero" href="sub-categoria">
                        <span uk-icon="icon:chevron-left; ratio:2"></span> <span class="padding-top-8">Volver</span>
                    </a>
                    </div>
                </div>
                <?=$buscador?>
                <div class="uk-width-auto@m">
                    <div class="uk-flex uk-flex-center"></div>
                </div>
            </div>
        </div>
    </section>

	<!--MENU CATEGORIAS 'tenemos todo lo necesario'-->
	<?=$menuCategorias?>
	
	<!--LISTA DE PRODUTOS-->
	<section style="">
		<div class="uk-container aling-center">
			<div class="aling-center margin-top-20 padding-30-0" id="loUltimo"
			style="background:#fff; width:100%">
				<h1 class="uk-text-center title ">Maquina de Hielo</h1>
				<h6 class="font-bold cero color-naranja text-xl"> Dispensadoras para hotel </h6>
				<p class="padding-20-100">
					Diseñadas para abastecer hielos en cubetas a huéspedes de hoteles. También funcionan de manera excelente en fábricas e instituciones que requieren constantemente de hielo. El diseño previene desperdicios y mermas.
				</p>

				<!--LISTA-->
				<div class="uk-container aling-center">
					<div class="uk-child-width-1-2@s uk-child-width-1-3@m uk-text-center" uk-grid>
						<div class="uk-flex uk-flex-center uk-text-center" uk-grid>
							<a class="container-product" href="">
						    <div class="padding-30-26">
							    <img src="./img/design/verMas.png">
							    <h6> Manitowoc Ice Sotto UG 40 </h6>
							    <p class="text-8">
							        Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
							    </p>
							    <button class="uk-button uk-button-default button-border margin-bottom-50">Cotizar</button>
						    </div>
							</a>
						</div>
						<div class="uk-flex uk-flex-center uk-text-center" uk-grid>
							<a class="container-product" href="">
						    <div class="padding-30-26">
							    <img src="./img/design/verMas.png">
							    <h6> Manitowoc Ice Sotto UG 40 </h6>
							    <p class="text-8">
							        Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
							    </p>
							    <button class="uk-button uk-button-default button-border margin-bottom-50">Cotizar</button>
						    </div>
							</a>
						</div>
						<div class="uk-flex uk-flex-center uk-text-center" uk-grid>
							<a class="container-product" href="">
						    <div class="padding-30-26">
							    <img src="./img/design/verMas.png">
							    <h6> Manitowoc Ice Sotto UG 40 </h6>
							    <p class="text-8">
							        Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
							    </p>
							    <button class="uk-button uk-button-default button-border margin-bottom-50">Cotizar</button>
						    </div>
							</a>
						</div>
						<div class="uk-flex uk-flex-center uk-text-center" uk-grid>
							<a class="container-product" href="">
						    <div class="padding-30-26">
							    <img src="./img/design/verMas.png">
							    <h6> Manitowoc Ice Sotto UG 40 </h6>
							    <p class="text-8">
							        Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
							    </p>
							    <button class="uk-button uk-button-default button-border margin-bottom-50">Cotizar</button>
						    </div>
							</a>
						</div>
						<div class="uk-flex uk-flex-center uk-text-center" uk-grid>
							<a class="container-product" href="">
						    <div class="padding-30-26">
							    <img src="./img/design/verMas.png">
							    <h6> Manitowoc Ice Sotto UG 40 </h6>
							    <p class="text-8">
							        Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
							    </p>
							    <button class="uk-button uk-button-default button-border margin-bottom-50">Cotizar</button>
						    </div>
							</a>
						</div>
						<div class="uk-flex uk-flex-center uk-text-center" uk-grid>
							<a class="container-product" href="">
						    <div class="padding-30-26">
							    <img src="./img/design/verMas.png">
							    <h6> Manitowoc Ice Sotto UG 40 </h6>
							    <p class="text-8">
							        Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
							    </p>
							    <button class="uk-button uk-button-default button-border margin-bottom-50">Cotizar</button>
						    </div>
							</a>
						</div>

					</div>
				</div>
				<!--LISTA-->
			</div>
		</div>
	</section>

	<!--UN PLAN-->
	<section class="uk-container padding-70-0" id="plan">
		<div class="cohete" style="
		position: absolute;left:0;">
			<img style="
			height: 480px;
			float:left;
			margin:0;
			padding:0;
			left:-50px;" src="./img/design/cohete.png">
		</div>
		<div class="uk-grid-divider uk-child-width-expand@s margin-left-0 padding-30-0" uk-grid>
		    <div class="aling-center">
		    	<h1 class="title padding-top-20 padding-bottom-20">
		    		Hagamos un plan</h1>
		    	<h6 class="font-bold padding-bottom-10">¡Recibe asesoría gratuita!</h6>
				Si estás buscando abrir un restaurante, una nueva sucursal o simplemente necesitas ampliar tu capacidad de producción, nosotros podemos ayudarte a llegar hasta el espacio sideral.
				<button class="uk-button uk-button-default button-border margin-top-30">¡Comencemos!</button>
			</div>
		    <div class="padding-top-10">
		    	<div uk-grid  class="margin-left-0 cero margin-top-menos-20" style="margin-top:-5px">
				    <div class="uk-text-center margin-left-0 number">1</div>
				    <div class="uk-width-expand padding-top-30">
				    	Cuentanos,
						<h6 class="font-bold padding-bottom-10">¿Qué tienes en mente?</h6>
				    </div>
				</div>
				<div uk-grid  class="margin-left-0 cero margin-top-menos-20" style="margin-top:-5px">
				    <div class="uk-text-center margin-left-0 number">2</div>
				    <div class="uk-width-expand padding-top-30">
				    	Cuentanos,
						<h6 class="font-bold padding-bottom-10">¿Qué tienes en mente?</h6>
				    </div>
				</div>
				<div uk-grid  class="margin-left-0 cero margin-top-menos-20" style="margin-top:-5px">
				    <div class="uk-text-center margin-left-0 number cero">3</div>
				    <div class="uk-width-expand padding-top-30">
				    	Cuentanos,
						<h6 class="font-bold padding-bottom-10">¿Qué tienes en mente?</h6>
				    </div>
				</div>
		    </div>
		</div>
	</section>


<?=$footer?>

<?=$scriptGNRL?>

</body>
</html>