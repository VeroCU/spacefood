<!DOCTYPE html>
<?=$headGNRL?>
<body>
<?=$header?>
	<!--header-->
	<section class="uk-container uk-container-large aling-center" id="menu2">
		<div class="cero uk-text-center" uk-grid>
			<div class="cero uk-width-expand@m">
			       <div class="cero uk-flex uk-flex-center">
			        <a class="cero" href="Inicio">
				        <img class="logo-header"  src="./img/design/logo-footer.png">
				       </a>
				</div>
			</div>
		</div>
	</section>
	<!--banner-->
	<section class="banner-header img1 margin-bottom-50" style="height:180px">
	</section>

	<!--BUSCAR-->
    <section style="">
        <div class="uk-container uk-container-large aling-center" id="menu2">
            <div class="uk-text-center" uk-grid>
                <div class="uk-width-auto@m">
                    <div class="uk-flex uk-flex-center">
                    <a class="cero" href="Inicio">
                        <span uk-icon="icon:chevron-left; ratio:2"></span> <span class="padding-top-8">Volver</span>
                    </a>
                    </div>
                </div>
                <?=$buscador?>
                <div class="uk-width-auto@m">
                    <div class="uk-flex uk-flex-center"></div>
                </div>
            </div>
        </div>
    </section>

	<!--MENU CATEGORIAS 'tenemos todo lo necesario'-->
	<?=$menuCategorias?>
	
	<!--SLIDER BANNER-->
	<section class="aling-center" id="slide2"
	style="background:#f1f2f2; width:100%">
		<div class="uk-position-relative uk-visible-toggle uk-dark" tabindex="-1" uk-slideshow="ratio: 7:3; animation: push">

		    <ul class="uk-slideshow-items" style="height:700px;">
		        <li> <div class="img1 picture-1"><button class="uk-button uk-button-default button-border margin-top-30">Cotizar</button></div> </li>
		        <li> <div class="img1 picture-2"><button class="uk-button uk-button-default button-border margin-top-30">Cotizar</button></div> </li>
		        <li> <div class="img1 picture-3"><button class="uk-button uk-button-default button-border margin-top-30">Cotizar</button></div> </li>
		    </ul>

		    <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"> </a>
		    <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"> </a>
		</div>
	</section>

		<!--PLAN-->
	<?=$hagamosUnPlan?>

<?=$footer?>

<?=$scriptGNRL?>

</body>
</html>