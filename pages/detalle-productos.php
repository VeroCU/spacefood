<!DOCTYPE html>
<?=$headGNRL?>
<body>
<?=$header?>
	<!--header-->
	<section class="uk-container uk-container-large aling-center" id="menu2">
		<div class="cero uk-text-center" uk-grid>
			<div class="cero uk-width-expand@m">
			       <div class="cero uk-flex uk-flex-center">
			        <a class="cero" href="Inicio">
				        <img class="logo-header"  src="./img/design/logo-footer.png">
				       </a>
				</div>
			</div>
		</div>
	</section>
	<!--banner-->
	<section class="banner-header img1 margin-bottom-50" style="height:180px">
	</section>

	<!--BUSCAR-->
    <section style="">
        <div class="uk-container uk-container-large aling-center" id="menu2">
            <div class="uk-text-center" uk-grid>
                <div class="uk-width-auto@m">
                    <div class="uk-flex uk-flex-center">
                    <a class="cero" href="productos">
                        <span uk-icon="icon:chevron-left; ratio:2"></span> <span class="padding-top-8">Volver</span>
                    </a>
                    </div>
                </div>
                <?=$buscador?>
                <div class="uk-width-auto@m">
                    <div class="uk-flex uk-flex-center"></div>
                </div>
            </div>
        </div>
    </section>
	
	<!--MENU CATEGORIAS 'tenemos todo lo necesario'-->
	<?=$menuCategorias?>

	<!--DETALLE-->
	<div class="uk-container aling-center">
		<div class="uk-child-width-expand@s margin-left-0" uk-grid>
		    <div class="uk-grid-item-match">
		        <img class="padding-10" style="height:80%" src="./img/design/freidora.png">
		    </div>
		    <div class="margin-top-20 padding-20-20">
		        <h6 class="font-bold cero color-naranja text-xl">
		        	Manitowoc Ice Sotto UG 40
		        </h6>
		        <p class="padding-20-20">
		            Freidora abierta a gas o eléctrica con opción de 1 a 4 tinas. Cuenta con computadora programable, sistema de alarmas cuando los productos están listos, sistema de filtrado automático y certificado Energy Star.
		            <br><br>
					Esta diseñada para ahorrar aceite y filtrar todos los residuos en el menor tiempo posible.
		        </p>
		        <div class="uk-child-width-expand uk-text-center padding-left-50 margin-top-20" uk-grid>
				    <div class="uk-flex padding-10">
				        <img data-src="./img/design/freidora.png" width="auto" height="auto" alt="" uk-img>
				    </div>
				    <div class="uk-flex padding-10">
				        <img data-src="./img/design/venta-equipo.png" width="auto" height="auto" alt="" uk-img>
				    </div>
				    <div class="uk-flex padding-10">
				        <img data-src="./img/design/verMas.png" width="auto" height="auto" alt="" uk-img>
				    </div>
				</div>
				<!--GALERIA-->
				<div class="uk-align-right">
					<div uk-lightbox class="btn-galeria">
					    <a class="uk-button btn-galeria uk-text-rigth uk-align-right galeria" href="./img/design/freidora.png"> Ver mas <span uk-icon="icon: chevron-right; ratio:1.5"></span> </a>
					    <a class="uk-button galeria" href="./img/design/necesito.png"></a>
					    <a class="uk-button galeria" href="./img/design/banner-2.png"></a>
					    <a class="uk-button galeria" href="./img/design/banner-1.jpg"></a>
					</div>
				</div>
				<!--GALERIA-->
				<div class=" uk-width-expand uk-flex uk-flex-center aling-center">
					<button class="uk-button uk-button-default button-border">Cotizar</button>
				</div>
		    </div>
		</div>
	</div>

	<!--SLIDER-->
	<section style="">
		<div class="uk-container aling-center" id="menu2 slider-detalle">
			<div class="aling-center margin-top-20 padding-30-0" id="loUltimo"
			style="background:#fff; width:100%">
				<h1 class="uk-text-center title">Completa tu proyeto con</h1>
				<div class="uk-position-relative uk-visible-toggle uk-dark aling-center" tabindex="-1" uk-slider style="height:560px;">
				    <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@m aling-center" style="margin:0 2em; height:500px;">
				        <li uk-align-center>
				        <a href="detalle-productos">
				            <div class="uk-position-center uk-panel padding-20-0">
				            	<img src="./img/design/verMas.png">
				            	<h6> Manitowoc Ice Sotto UG 40 </h6>
				            	<p>
				            		Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
				            		
				            	</p>
				            </div>
				        </a>
				        </li>
				        <li uk-align-center>
				        <a href="detalle-productos">
				            <div class="uk-position-center uk-panel padding-20-0">
				            	<img src="./img/design/verMas.png">
				            	<h6> Manitowoc Ice Sotto UG 40 </h6>
				            	<p>
				            		Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
				            		
				            	</p>
				            </div>
				        </a>
				        </li>
				        <li uk-align-center>
				        <a href="detalle-productos">
				            <div class="uk-position-center uk-panel padding-20-0">
				            	<img src="./img/design/verMas.png">
				            	<h6> Manitowoc Ice Sotto UG 40 </h6>
				            	<p>
				            		Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
				            		
				            	</p>
				            </div>
				        </a>
				        </li>
				        <li uk-align-center>
				        <a href="detalle-productos">
				            <div class="uk-position-center uk-panel padding-20-0">
				            	<img src="./img/design/verMas.png">
				            	<h6> Manitowoc Ice Sotto UG 40 </h6>
				            	<p>
				            		Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
				            	</p>
				            </div>
				        </a>
				        </li>
				    </ul>
				    <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
				    <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
				</div>
			</div>
		</div>
	</section>

	<!--UN PLAN-->
	<section class="uk-container padding-bottom-80" id="plan">
		<div class="cohete" style="
		position: absolute;left:0;">
			<img style="
			height: 480px;
			float:left;
			margin:0;
			padding:0;
			left:-50px;" src="./img/design/cohete.png">
		</div>
		<div class="uk-grid-divider uk-child-width-expand@s margin-left-0 padding-30-0" uk-grid>
		    <div class="aling-center">
		    	<h1 class="title padding-top-20 padding-bottom-20">
		    		Hagamos un plan</h1>
		    	<h6 class="font-bold padding-bottom-10">¡Recibe asesoría gratuita!</h6>
				Si estás buscando abrir un restaurante, una nueva sucursal o simplemente necesitas ampliar tu capacidad de producción, nosotros podemos ayudarte a llegar hasta el espacio sideral.
				<button class="uk-button uk-button-default button-border margin-top-30">¡Comencemos!</button>
			</div>
		    <div class="padding-top-10">
		    	<div uk-grid  class="margin-left-0 cero margin-top-menos-20" style="margin-top:-5px">
				    <div class="uk-text-center margin-left-0 number">1</div>
				    <div class="uk-width-expand padding-top-30">
				    	Cuentanos,
						<h6 class="font-bold padding-bottom-10">¿Qué tienes en mente?</h6>
				    </div>
				</div>
				<div uk-grid  class="margin-left-0 cero margin-top-menos-20" style="margin-top:-5px">
				    <div class="uk-text-center margin-left-0 number">2</div>
				    <div class="uk-width-expand padding-top-30">
				    	Cuentanos,
						<h6 class="font-bold padding-bottom-10">¿Qué tienes en mente?</h6>
				    </div>
				</div>
				<div uk-grid  class="margin-left-0 cero margin-top-menos-20" style="margin-top:-5px">
				    <div class="uk-text-center margin-left-0 number cero">3</div>
				    <div class="uk-width-expand padding-top-30">
				    	Cuentanos,
						<h6 class="font-bold padding-bottom-10">¿Qué tienes en mente?</h6>
				    </div>
				</div>
		    </div>
		</div>
	</section>


<?=$footer?>

<?=$scriptGNRL?>

</body>
</html>