<!DOCTYPE html>
<?=$headGNRL?>
<body>
<?=$header?>
	<!--header-->
	<section class="uk-container uk-container-large aling-center" id="menu2">
		<div class="cero uk-text-center" uk-grid>
			<div class="cero uk-width-expand@m">
			       <div class="cero uk-flex uk-flex-center">
			        <a class="cero" href="Inicio">
				        <img class="logo-header"  src="./img/design/logo-footer.png">
				       </a>
				</div>
			</div>
		</div>
	</section>
	<!--banner-->
	<section class="banner-header img1 margin-bottom-50" style="height:180px">
	</section>

	<!--VOLVER-->
	<section style="">
		<div class="uk-container uk-container-large aling-center" id="menu2">
			<div class="uk-text-center" uk-grid>
			    <div class="uk-width-auto@m">
			        <div class="uk-flex uk-flex-center">
			        	<a class="cero" href="Inicio">
	                        <span uk-icon="icon:chevron-left; ratio:2"></span> <span class="padding-top-8">Volver</span>
	                    </a>
			        </div>
			    </div>
			    <div class="uk-width-expand@m">
			        <div class="uk-flex uk-flex-center">
			        	<div class="uk-margin">
						</div>
			        </div>
			    </div>
			    <div class="uk-width-auto@m">
			        <div class="uk-flex uk-flex-center"></div>
			    </div>
			</div>
		</div>
	</section>

	<!--CASOS-->
	<section  class="uk-container padding-70-0" id="plan">
			<div class="uk-flex aling-center">
				<div uk-grid class="padding-0-60">
					<p class="title-2 uk-align-center">
						Casos de <span class="title">éxito</span>.
					</p>
					<p class="cero uk-flex uk-align-center padding-0-60 margin-top-menos-20">
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.
					</p>
				</div>
			</div>
	</section>

	<!--UN PLAN-->
	<section  style="background-color:#eff1f1">
		<div class="uk-container padding-70-0" id="plan">
			<div class="cohete" style="
			position: absolute;left:0;">
				<img style="
				height: 480px;
				float:left;
				margin:0;
				padding:0;
				left:-50px;" src="./img/design/cohete.png">
			</div>
			<div class="uk-flex aling-center">
				<div uk-grid class="padding-0-60">
					<p class="title-2 uk-align-center">
						<span class="title">Soluciones</span> de otro mundo<br>
						para la <span class="title">industria de alimentos</span> de hoy.
					</p>
					<p class="cero uk-flex uk-align-center padding-0-60 margin-top-menos-20">
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit.
					</p>
				</div>
			</div>
		</div>
	</section>

	<!--SLIDER-->
	<section style="">
		<div class="uk-container aling-center" id="menu2">
			<div class="aling-center margin-top-20 padding-30-0" id="loUltimo"
			style="background:#fff; width:100%">
				<h1 class="uk-text-center title ">Lo último para tu cocina</h1>
				<div class="uk-position-relative uk-visible-toggle uk-dark aling-center" tabindex="-1" uk-slider style="height:560px;">
				    <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@m aling-center" style="margin:0 2em; height:500px;">
				        <li uk-align-center>
				            <div class="uk-position-center uk-panel padding-20-0">
				            	<a href="detalle-productos">
				            		<img src="./img/design/verMas.png">
					            	<h6> Manitowoc Ice Sotto UG 40 </h6>
					            	<p class="color-p">
					            		Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
					            		
					            	</p>
					            </a>
				            	<a href="" class="uk-button uk-button-default button-border">Cotizar</a>
				            </div>
				        </li>
				        <li uk-align-center>
				            <div class="uk-position-center uk-panel padding-20-0">
				            	<a href="detalle-productos">
				            		<img src="./img/design/verMas.png">
					            	<h6> Manitowoc Ice Sotto UG 40 </h6>
					            	<p class="color-p">
					            		Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
					            		
					            	</p>
					            </a>
				            	<a href="" class="uk-button uk-button-default button-border">Cotizar</a>
				            </div>
				        </li>
				        <li uk-align-center>
				            <div class="uk-position-center uk-panel padding-20-0">
				            	<a href="detalle-productos">
				            		<img src="./img/design/verMas.png">
					            	<h6> Manitowoc Ice Sotto UG 40 </h6>
					            	<p class="color-p">
					            		Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
					            		
					            	</p>
					            </a>
				            	<a href="" class="uk-button uk-button-default button-border">Cotizar</a>
				            </div>
				        </li>
				        <li uk-align-center>
				            <div class="uk-position-center uk-panel padding-20-0">
				            	<a href="detalle-productos">
				            		<img src="./img/design/verMas.png">
					            	<h6> Manitowoc Ice Sotto UG 40 </h6>
					            	<p class="color-p">
					            		Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
					            		
					            	</p>
					            </a>
				            	<a href="" class="uk-button uk-button-default button-border">Cotizar</a>
				            </div>
				        </li>
				        <li uk-align-center>
				            <div class="uk-position-center uk-panel padding-20-0">
				            	<a href="detalle-productos">
				            		<img src="./img/design/verMas.png">
					            	<h6> Manitowoc Ice Sotto UG 40 </h6>
					            	<p class="color-p">
					            		Maquina para fabricar hielo marca Manitowoc Ice modelo RNS20A tipo sobremostrador con producción máxima de 45 kg cada 24 horas.
					            		
					            	</p>
					            </a>
				            	<a href="" class="uk-button uk-button-default button-border">Cotizar</a>
				            </div>
				        </li>
				    </ul>
				    <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
				    <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
				</div>
				<div class="padding-bottom-50">
					<a href="categoria" class="uk-button uk-button-default button-border">Ver más!</a>
				</div>
			</div>
		</div>
	</section>
	
<?=$footer?>

<?=$scriptGNRL?>

</body>
</html>